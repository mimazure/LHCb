/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "Event/CaloAdc.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
#include "Event/ProtoParticle.h"
#include "Event/RawBankReadoutStatus.h"
#include "GaudiKernel/IRegistry.h"
#include <algorithm>

namespace LHCb::Calo::Utilities {

  using details::contains_ci;
  namespace {
    std::string pathFromContext( const std::initializer_list<std::pair<std::string_view, std::string_view>>& table,
                                 std::string_view                                                            val ) {
      auto i = std::find_if(
          std::begin( table ), std::end( table ),
          [&]( const std::pair<std::string_view, std::string_view>& item ) { return contains_ci( val, item.first ); } );
      return i != std::end( table ) ? std::string{i->second} : std::string{};
    }
  } // namespace

  // Default location for CaloFutureObject as function of detector

  // DetectorElement location from name
  std::string DeCaloFutureLocation( std::string_view name ) {
    auto det = CaloIndexFromAlg( name );
    return det == CaloCellCode::CaloIndex::EcalCalo
               ? DeCalorimeterLocation::Ecal
               : det == CaloCellCode::CaloIndex::HcalCalo
                     ? DeCalorimeterLocation::Hcal
                     : det == CaloCellCode::CaloIndex::PrsCalo
                           ? DeCalorimeterLocation::Prs
                           : det == CaloCellCode::CaloIndex::SpdCalo ? DeCalorimeterLocation::Spd : "";
  }

  // ADC location from name
  std::string CaloFutureRawBankReadoutStatusLocation( std::string_view name ) {
    auto det = CaloIndexFromAlg( name );
    using namespace RawBankReadoutStatusLocation;
    return det == CaloCellCode::CaloIndex::EcalCalo
               ? RawBankReadoutStatusLocation::Ecal
               : det == CaloCellCode::CaloIndex::HcalCalo ? RawBankReadoutStatusLocation::Hcal : "";
  }

  // ADC location from name
  std::string CaloFutureAdcLocation( std::string_view name ) {
    auto det = CaloIndexFromAlg( name );
    using namespace CaloAdcLocation;
    return det == CaloCellCode::CaloIndex::EcalCalo ? Ecal : det == CaloCellCode::CaloIndex::HcalCalo ? Hcal : "";
  }

  // Digit location from name
  std::string CaloFutureDigitLocation( std::string_view name ) {
    auto det = CaloIndexFromAlg( name );
    using namespace CaloDigitLocation;
    return det == CaloCellCode::CaloIndex::EcalCalo
               ? Ecal
               : det == CaloCellCode::CaloIndex::HcalCalo
                     ? Hcal
                     : det == CaloCellCode::CaloIndex::PrsCalo
                           ? Prs
                           : det == CaloCellCode::CaloIndex::SpdCalo ? Spd : Ecal; // default is Ecal in
                                                                                   // offline mode
  }

  // Digit location from name
  std::string CaloFutureUnfilteredDigitLocation( std::string_view name ) {
    auto det = CaloIndexFromAlg( name );
    using namespace CaloDigitLocation;
    return det == CaloCellCode::CaloIndex::EcalCalo
               ? UnfilteredEcal
               : det == CaloCellCode::CaloIndex::HcalCalo ? UnfilteredHcal
                                                          : UnfilteredEcal; // default is Ecal in offline mode
  }

  // Cluster location from context
  std::string CaloFutureSplitClusterLocation() { return CaloClusterLocation::EcalSplit; }

  std::string CaloFutureClusterLocation( std::string_view name, std::string_view type ) {
    //##  splitClusters
    if ( contains_ci( name, "SPLITCLUSTER" ) ) return CaloFutureSplitClusterLocation();

    // ## standard clusters
    CaloCellCode::CaloIndex det = CaloIndexFromAlg( name );

    using namespace CaloClusterLocation;
    std::string clusters;
    if ( det == CaloCellCode::CaloIndex::HcalCalo ) {
      clusters = Hcal;
    } else {
      if ( type == "EcalRaw" ) {
        clusters = EcalRaw;
      } else if ( type == "EcalOverlap" ) {
        clusters = EcalOverlap;
      } else {
        clusters = Ecal;
      }
    }
    return clusters;
  }

  // Hypo location from type
  std::string CaloFutureHypoLocation( std::string_view type ) {
    using namespace CaloHypoLocation;
    static const std::initializer_list<std::pair<std::string_view, std::string_view>> table = {
        {"ELECTRON", Electrons},
        {"SPLITPHOTON", SplitPhotons},
        {"BREM", Photons},   // Brem=photon
        {"PHOTON", Photons}, // after split
        {"MERGED", MergedPi0s}};
    return pathFromContext( table, type );
  }

  // CaloFutureId location from type
  std::string CaloFutureIdLocation( std::string_view type ) {
    using namespace CaloFutureIdLocation;
    static const std::initializer_list<std::pair<std::string_view, std::string_view>> table = {
        {"BREMMATCH", BremMatch},
        {"ELECTRONMATCH", ElectronMatch},
        {"PHOTONMATCH", ClusterMatch}, //=ClusterMatch
        {"CLUSTERMATCH", ClusterMatch},
        {"BREMCHI2", BremChi2},
        {"ECALCHI2", EcalChi2},
        {"CLUSCHI2", ClusChi2},
        {"ECALE", EcalE},
        {"HCALE", HcalE},
        {"PRSE", PrsE},
        {"SPDE", SpdE},
        {"PRSPIDE", PrsPIDe},
        {"BREMPIDE", BremPIDe},
        {"ECALPIDE", EcalPIDe},
        {"HCALPIDE", HcalPIDe},
        {"ECALPIDMU", EcalPIDmu},
        {"HCALPIDMU", HcalPIDmu},
        {"INBREM", InBrem},
        {"INECAL", InEcal},
        {"INHCAL", InHcal},
        {"INPRS", InPrs},
        {"INSPD", InSpd},
        {"PHOTONFROMMERGEDID", PhotonFromMergedID},
        {"SPLITPHOTONID", PhotonFromMergedID}, // same
        {"PHOTONID", PhotonID},                // after SplitPhotonID
        {"MERGEDID", MergedID},                // after PhotonFromMerged
    };
    return pathFromContext( table, type );
  }

  // Track location
  std::vector<std::string> TrackLocations() {
    return {TrackLocation::Default}; // default is offline
  }

  const CaloCluster* ClusterFromHypo( const CaloHypo& hypo, bool split ) {
    if ( 1 == hypo.clusters().size() ) return hypo.clusters().front(); // single hypo

    const auto& clusters = hypo.clusters();
    // split hypo
    if ( hypo.parent() && hypo.parent()->registry() ) {
      bool hsplit =
          ( contains_ci( hypo.parent()->registry()->identifier(), "SPLIT" ) && 1 < clusters.size() ); // split hypo
      if ( !hsplit ) return clusters.front(); // SHOULD NEVER OCCUR
      const CaloCluster* cluster = nullptr;
      for ( const auto& cl : clusters ) {
        bool split_container = contains_ci( cl->parent()->registry()->identifier(), "SPLIT" );
        if ( split && split_container )
          cluster = cl; // return splitCluster
        else if ( !split && !split_container )
          cluster = cl; // return mainCluster
      }
      return cluster;
    }
    // unregistered caloHypo -> return the smallest/largest cluster according to getsplit
    auto smallest = []( const CaloCluster* lhs, const CaloCluster* rhs ) {
      return lhs->entries().size() < rhs->entries().size();
    };
    auto largest = []( const CaloCluster* lhs, const CaloCluster* rhs ) {
      return lhs->entries().size() > rhs->entries().size();
    };
    auto i = std::min_element( clusters.begin(), clusters.end(),
                               split ? smallest : largest ); // only works because non-capturing lambda can be converted
                                                             // to pointer to function
    return i != clusters.end() ? i->target() : nullptr;
  }

} // namespace LHCb::Calo::Utilities
