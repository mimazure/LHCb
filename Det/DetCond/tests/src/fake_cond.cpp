/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Core/ConditionConverter.h>
#include <Core/ConditionsRepository.h>
#include <Core/yaml_converters.h>

#include "DetDesc/ParamValidDataObject.h"

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/Detector.h"
#include "DD4hep/Factories.h"

#include "XML/Conversions.h"
#include "XML/XML.h"

#include <memory>

/// very simple plugin registering an hardcoded condition directly to /world element
static long create_conditions_recipes( dd4hep::Detector& description, xml_h /*e*/ ) {
  auto world    = description.world();
  auto requests = std::make_shared<LHCb::Detector::ConditionsRepository>();

  LHCb::YAMLConverters::set_converter( "!testcond", []( std::string_view name, const YAML::Node& cond_data ) {
    dd4hep::Condition c{std::string{name}, "testcond"};
    auto*             value = &c.bind<ParamValidDataObject>();
    for ( const auto& param : cond_data ) {
      const auto param_name = param.first.as<std::string>();
      const auto val        = param.second.as<double>();
      value->addParam<double>( param_name, val );
    }
    return c;
  } );

  requests->addLocation( world, dd4hep::ConditionKey::itemCode( "TestCondition" ), "conditions.yml", "TestCondition" );
  using Ext_t = std::shared_ptr<LHCb::Detector::ConditionsRepository>;
  world.addExtension( new dd4hep::detail::DeleteExtension<Ext_t, Ext_t>( new Ext_t( requests ) ) );
  return 1;
}
DECLARE_XML_DOC_READER( Fake_cond, create_conditions_recipes )

#include "DD4hep/Grammar.h"
namespace dd4hep {
  template <>
  const BasicGrammar& BasicGrammar::instance<ParamValidDataObject>() {
    static Grammar<ParamValidDataObject> gr;
    return gr;
  }
} // namespace dd4hep

/// Specialized conversion of Param entities
template <>
void dd4hep::Converter<ParamValidDataObject>::operator()( xml_h element ) const {
  double              value;
  const BasicGrammar& g = BasicGrammar::instance<double>();
  if ( !g.fromString( &value, element.text() ) ) g.invalidConversion( element.text(), g.type() );
  _option<ParamValidDataObject>()->addParam<double>( element.attr<std::string>( _U( name ) ), value );
}

struct FakeConditionConverter : LHCb::Detector::ConditionConverter {
  virtual dd4hep::Condition operator()( dd4hep::Detector& description, LHCb::Detector::ConditionIdentifier*,
                                        xml_h             element ) const override {
    dd4hep::Condition                       cond( element.attr<std::string>( _U( name ) ), "param" );
    auto*                                   value = &cond.bind<ParamValidDataObject>();
    dd4hep::Converter<ParamValidDataObject> conv( description, 0, value );
    xml_coll_t( element, "param" ).for_each( conv );
    return cond;
  }
};

static void* create_conditions_converter_fake( dd4hep::Detector&, int, char** ) { return new FakeConditionConverter(); }

DECLARE_DD4HEP_CONSTRUCTOR( LHCb_ConditionsConverter_666, create_conditions_converter_fake )
