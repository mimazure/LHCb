/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LbDD4hep/IDD4hepSvc.h"

#include "Gaudi/Property.h"
#include "GaudiAlg/FixTESPath.h"
#include "GaudiAlg/FunctionalUtilities.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/ServiceHandle.h"

namespace {
  template <typename Transform>
  struct construct_constructor;

  template <typename Ret, typename... Args>
  struct construct_constructor<Ret( Args const&... )> {
    static_assert( std::is_constructible_v<Ret, Args const&...> );
    [[nodiscard]] static constexpr auto
    construct( Args const&... args ) noexcept( std::is_nothrow_constructible_v<Ret, Args const&...> ) {
      return Ret{args...};
    }
  };

  template <typename Transform>
  constexpr auto invoke_constructor = construct_constructor<Transform>::construct;

} // namespace

namespace LHCb::Det::LbDD4hep {

  using ConditionContext = IDD4hepSvc::DD4HepSlicePtr;

  template <typename T>
  class ConditionAccessorBase {

    template <typename... Args, std::size_t... Is>
    ConditionAccessorBase( const std::tuple<Args...>& args, std::index_sequence<Is...> )
        : ConditionAccessorBase( std::get<Is>( args )... ) {}

  public:
    template <typename Owner>
    ConditionAccessorBase( Owner* owner, const std::string& keyName, const std::string& keyDefault,
                           const std::string& keyDoc = "" )
        : m_key{owner, keyName, keyDefault, keyDoc} {}

    template <typename... Args>
    ConditionAccessorBase( const std::tuple<Args...>& args )
        : ConditionAccessorBase( args, std::index_sequence_for<Args...>{} ) {}

    // Condition accessors can neither be moved nor copied
    ConditionAccessorBase( const ConditionAccessorBase& ) = delete;
    ConditionAccessorBase( ConditionAccessorBase&& )      = delete;
    ConditionAccessorBase& operator=( const ConditionAccessorBase& ) = delete;
    ConditionAccessorBase& operator=( ConditionAccessorBase&& ) = delete;

    const std::string& key() const { return m_key; }

  protected:
    dd4hep::Condition::key_type getKey() const {
      auto colonPos = m_key.value().find_first_of( ':' );
      if ( colonPos == std::string::npos ) { throw "Expected a ':' in condition key"; }
      auto                           path = m_key.value().substr( 0, colonPos );
      auto                           hash = dd4hep::ConditionKey::itemCode( m_key.value().substr( colonPos + 1 ) );
      dd4hep::ConditionKey::KeyMaker m( dd4hep::detail::hash32( path ), hash );
      return m.hash;
    }

    // Configurable key which this ConditionAccessor points to.
    Gaudi::Property<std::string> m_key;
  };

  template <typename T>
  struct ConditionAccessor : ConditionAccessorBase<T> {
    using ConditionAccessorBase<T>::ConditionAccessorBase;
    const T get( const ConditionContext& ctx ) const {
      const dd4hep::Condition& a = ctx->pool->get( this->getKey() );
      return a.get<T>();
    }
  };

  /// specialization of ConditionAccessor in case of dd4hep Handle
  /// In such case the pool get returns a Condition, and we have to use the internal
  /// "safe cast" mechanism to convert it into the returned handle
  template <template <typename> class HANDLE, typename T> //,
  //           typename std::enable_if<std::is_base_of_v<dd4hep::Handle<T>, HANDLE<T>>>::type* = nullptr>
  struct ConditionAccessor<HANDLE<T>> : ConditionAccessorBase<HANDLE<T>> {
    using ConditionAccessorBase<HANDLE<T>>::ConditionAccessorBase;
    const HANDLE<T> get( const ConditionContext& ctx ) const { return HANDLE<T>( ctx->pool->get( this->getKey() ) ); }
  };

  template <typename Base>
  class ConditionAccessorHolder : public Base {

  public:
    /// Forward to base class constructor
    using Base::Base;

    /// Helper to expose this class to specializations without having to spell
    /// the whole name (see Gaudi::Examples::Conditions::UserAlg)
    using base_class = ConditionAccessorHolder<Base>;

  public:
    const dd4hep::Detector& getDetector() { return m_dd4hepSvc->getDetector(); }

    template <typename Transform, size_t N = detail::arity_v<Transform>>
    bool addConditionDerivation( const std::array<std::string, N>& inputKeys, const std::string& outputKey,
                                 Transform f = invoke_constructor<Transform> ) const {
      auto func = IDD4hepSvc::DD4HepDerivationFunc{new GenericConditionUpdateCall( std::move( f ) )};
      return m_dd4hepSvc->add( make_span( begin( inputKeys ), end( inputKeys ) ), std::move( outputKey ), func );
    }

    const ConditionContext& getConditionContext( const EventContext& /* ctx */ ) const {
      // context is taken from thread local storage for the handles
      return *m_ctxHandle.get();
    }

  private:
    // We must declare a dependency on the condition context
    // This context is actually the DD4Hep IOV stored in the TES
    DataObjectReadHandle<ConditionContext> m_ctxHandle{DataObjID{IDD4hepSvc::DefaultSliceLocation}, this};

    ServiceHandle<IDD4hepSvc> m_dd4hepSvc{this, "DD4hepSvc", "LHCb::Det::LbDD4hep::DD4hepSvc",
                                          "underlying DD4Hep service handling conditions"};

    /// Helper to postpone the registration to the backend.
    ///
    /// In Gaudi properties are set during initialize, so, in this example implementation,
    /// the registration has to be delayed from construction time to initialization time.
    //    std::list<std::function<void()>> m_delayedRegistrations;
  };

  template <typename C, typename A>
  const C get( const ConditionAccessor<C>& handle, const ConditionAccessorHolder<A>& algo, const EventContext& ctx ) {
    return handle.get( algo.getConditionContext( ctx ) );
  }

  template <typename C>
  const std::string& getKey( const ConditionAccessor<C>& handle ) {
    return handle.key();
  }

  template <typename... C>
  struct useConditionHandleFor {
    template <typename T>
    using InputHandle = std::enable_if_t<std::disjunction_v<std::is_same<std::decay_t<T>, std::decay_t<C>>...>,
                                         ConditionAccessor<std::decay_t<T>>>;
  };

  template <typename Algorithm = Gaudi::Algorithm>
  using AlgorithmWithCondition = ConditionAccessorHolder<FixTESPath<Algorithm>>;

  template <typename... C>
  using usesConditions =
      Gaudi::Functional::Traits::use_<useConditionHandleFor<C...>,
                                      Gaudi::Functional::Traits::BaseClass_t<AlgorithmWithCondition<>>>;

  template <typename B, typename... C>
  using usesBaseAndConditions =
      Gaudi::Functional::Traits::use_<useConditionHandleFor<C...>,
                                      Gaudi::Functional::Traits::BaseClass_t<AlgorithmWithCondition<B>>>;
} // namespace LHCb::Det::LbDD4hep
