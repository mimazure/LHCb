/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/Detector.h"
#include "DD4hep/detail/ConditionsInterna.h"
#include "DDCond/ConditionsSlice.h"

#include "Kernel/STLExtensions.h"

#include "GaudiKernel/IService.h"

#include "boost/callable_traits.hpp"

#include <memory>
#include <type_traits>

namespace LHCb::Det::LbDD4hep {

  namespace detail {

    template <typename Callable>
    inline constexpr auto arity_v = std::tuple_size_v<boost::callable_traits::args_t<Callable>>;

    template <typename Input>
    decltype( auto ) fetch_1( dd4hep::cond::ConditionUpdateContext const& context, std::size_t n ) {
      auto cond = context.condition( context.key( n ), true );
      return cond.get<std::decay_t<Input>>();
    }

    template <typename TypeList, std::size_t... I>
    auto fetch_n( dd4hep::cond::ConditionUpdateContext const& ctx, std::index_sequence<I...> ) {
      return std::forward_as_tuple( fetch_1<std::tuple_element_t<I, TypeList>>( ctx, I )... );
    }

    template <typename Transform, std::ptrdiff_t N = detail::arity_v<Transform>>
    auto fetch_inputs_for( dd4hep::cond::ConditionUpdateContext const& ctx ) {
      using InputTypes = boost::callable_traits::args_t<Transform>;
      return fetch_n<InputTypes>( ctx, std::make_index_sequence<N>{} );
    }
  } // namespace detail

  /**
   * small functor wrapping a lambda and suitable for creating
   * a DD4hep derived condition
   */
  class GenericConditionUpdateCall : public dd4hep::cond::ConditionUpdateCall {
  public:
    /// Type for a user provided callback function.
    /// The first argument is the ConditionKey of the target and is used to be
    /// able to reuse a transformation function that behaves differently depending
    /// on the requested output, The ConditionUpdateContext will be filled with the
    /// input conditions, and the last argument is the Condition instance to update.
    using ConditionCallbackFunction =
        std::function<void( dd4hep::cond::ConditionUpdateContext const&, dd4hep::Condition& )>;
    // constructor, taking a transform and creating the 2 callables used for operator() and resolve
    // the constructor is templated and the callables' types are not, allowing a generic interface
    template <typename Transform>
    GenericConditionUpdateCall( Transform f ) {
      using ConditionType = decltype( std::apply(
          f, detail::fetch_inputs_for<Transform>( std::declval<dd4hep::cond::ConditionUpdateContext&>() ) ) );
      m_fillCondition     = [  =,
                         f = std::move( f )]( dd4hep::detail::ConditionObject*            condition,
                                              dd4hep::cond::ConditionUpdateContext const& ctx ) -> dd4hep::Condition {
        condition->data.bind<ConditionType>() = std::apply( f, detail::fetch_inputs_for<Transform>( ctx ) );
        return condition;
      };
    }
    struct MyProcessor : dd4hep::Condition::Processor {
      int process( dd4hep::Condition ) const override { return 0; }
    };
    dd4hep::Condition operator()( const dd4hep::ConditionKey&,
                                  dd4hep::cond::ConditionUpdateContext& context ) override {
      context.conditionsMap().scan( MyProcessor() );
      auto* cond = new dd4hep::detail::ConditionObject();
      return m_fillCondition( cond, context );
    }
    void resolve( dd4hep::Condition, dd4hep::cond::ConditionUpdateContext& ) override {}

  private:
    std::function<dd4hep::Condition( dd4hep::detail::ConditionObject*, dd4hep::cond::ConditionUpdateContext& )>
        m_fillCondition;
  };

  /** @class IDD4hepSvc IDD4hepSvc.h DetDesc/IDD4hepSvc.h
   *
   *  Definition of abstract interface for the service providing the DD4hep Geometry
   *
   */
  struct IDD4hepSvc : extend_interfaces<IService> {

    using DD4HepSlicePtr                       = std::shared_ptr<dd4hep::cond::ConditionsSlice>;
    using DD4HepDerivationFunc                 = std::shared_ptr<dd4hep::cond::ConditionUpdateCall>;
    static constexpr auto DefaultSliceLocation = "IOVLockDD4hep";

    /** Declaration of the unique interface identifier
     *  ( interface id, major version, minor version)
     */
    DeclareInterfaceID( IDD4hepSvc, 1, 0 );

    /**
     * get the DD4hep detector instance
     */
    virtual const dd4hep::Detector& getDetector() const = 0;

    /**
     * get the Condition slice from the cache
     */
    virtual DD4HepSlicePtr get_slice( size_t iov ) = 0;

    /**
     * Add a derived condition to DD4hep
     * @arg inputs the keys for the conditions the derived one depends on.
     *      These conditions will be given as input to the func functor
     * @arg output the key for the new derived condition
     * @arg func a functor able to create/update the derived condition
     *      out of the inputs
     */
    virtual bool add( LHCb::span<const std::string> inputs, const std::string& output, DD4HepDerivationFunc& func ) = 0;
  };

} // namespace LHCb::Det::LbDD4hep
