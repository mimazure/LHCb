/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DD4hepSvc.h"

#include "Core/ConditionsRepository.h"

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/Conditions.h"
#include "DD4hep/ConditionsPrinter.h"

#include "GaudiKernel/IMessageSvc.h"

#include "Core/ConditionsRepository.h"
#include "DD4hepSvc.h"

#include "TError.h"
#include "TGeoManager.h"
#include "TString.h"
#include "TSystem.h"

#include <map>

namespace {

  // make sure the the ROOT logging level is such that the creation of TGeoManager
  // does not log anything on stderr. The verbosity will then be set back to a
  // reasonnable value in DD4hepSvc constructor
  static auto _ = [] {
    TGeoManager::SetVerboseLevel( 0 );
    return 0;
  }();

  /**
   * Creates a dd4hep hash out of an LHCb condition path
   * The DD4hep has has 2 parts : top 32 bits are used to parition conditions
   * and lower 32bits are identifying the conditions themselves within a partition
   * Thus the condition paths in LHCb, when used with DD4hep have 2 parts
   * separated by a ':' and each part is hashed independently to form the
   * final hash
   */
  dd4hep::Condition::key_type makeKey( std::string const& s ) {
    auto colonPos = s.find_first_of( ':' );
    auto hi       = dd4hep::ConditionKey::itemCode( s.substr( 0, colonPos ) );
    auto low      = dd4hep::ConditionKey::itemCode( colonPos == std::string::npos ? "" : s.substr( colonPos + 1 ) );
    return ( ( (dd4hep::Condition::key_type)hi ) << 32 ) | ( low & 0xffffffff );
  }

} // namespace

namespace LHCb::Det::LbDD4hep {

  const std::map<int, int> ROOT_LEVEL_MAP{
      {kPrint, MSG::DEBUG}, {kInfo, MSG::INFO},      {kWarning, MSG::WARNING}, {kError, MSG::ERROR},
      {kBreak, MSG::FATAL}, {kSysError, MSG::FATAL}, {kFatal, MSG::FATAL},
  };

  int map_to_ROOT_level( int level ) {

    auto m = ROOT_LEVEL_MAP.find( level );
    if ( m != ROOT_LEVEL_MAP.end() ) {
      return m->second;
    } else {
      return MSG::WARNING;
    }
  }

  void handle_message_ROOT( int level, Bool_t /* abort */, const char* location, const char* msg ) {

    if ( gaudiMessageSvc ) {
      gaudiMessageSvc->reportMessage( location, map_to_ROOT_level( level ), msg );
    } else {
      std::cerr << "Unhandled: " << level << " : " << location << ":" << msg << std::endl;
    }
  }

  static size_t handle_message_DD4hep( void* /* par */, dd4hep::PrintLevel lvl, const char* src, const char* msg ) {
    IMessageSvc* svc = gaudiMessageSvc;
    if ( svc ) {
      if ( (int)lvl >= svc->outputLevel() ) {
        svc->reportMessage( src, lvl, msg );
        return strlen( msg );
      }
    } else {
      std::cerr << "Unhandled: " << lvl << " : " << src << ":" << msg << std::endl;
    }
    return 0;
  }

  StatusCode DD4hepSvc::initialize() {
    /// initialise the base class
    StatusCode statusCode = Service::initialize();
    if ( statusCode.isFailure() ) return statusCode; // Propagate the error

    /// set the list of detectorNames. Cannot be done in constructor as we use Properties
    m_detectorNames = m_detectorList.value();

    // Setting the verbosity level and redirecting the ROOT and DD4hep logs
    gaudiMessageSvc = msgSvc().get();
    TGeoManager::SetVerboseLevel( m_verboseLevel.value() );
    // Should we use the TGEoManager or get it from the description ?
    // m_description.manager().SetVerboseLevel( m_verboseLevel.value() );
    SetErrorHandler( handle_message_ROOT );
    dd4hep::setPrinter( (void*)msgSvc(), handle_message_DD4hep );

    /// Load the actual description from Detector
    // We use ROOT to expand the variables in the path as it has a convenient
    // function for that.
    TString descriptionFile( m_detDescLocation.value() );
    gSystem->ExpandPathName( descriptionFile );
    const char* fname = descriptionFile.Data();
    if ( this->msgLevel( MSG::DEBUG ) ) debug() << "Loading DD4hep Geometry: " << fname << endmsg;
    m_description.apply( "DD4hep_CompactLoader", 1, (char**)&fname );

    if ( this->msgLevel( MSG::DEBUG ) ) debug() << "Initializing the ConditionsManager " << endmsg;

    TString conditionsLocation( m_conditionsLocation.value() );
    gSystem->ExpandPathName( conditionsLocation );
    if ( this->msgLevel( MSG::DEBUG ) ) debug() << "Using conditions location: " << conditionsLocation.Data() << endmsg;
    DetectorDataService::initialize( conditionsLocation.Data() );

    return StatusCode::SUCCESS;
  }

  bool DD4hepSvc::add( LHCb::span<const std::string> inputs, const std::string& output, DD4HepDerivationFunc& func ) {
    // The condition path has 2 parts, separated by a ':'. This corresponds
    // to the 2 parts od the DD4hep hash. Pratically the first one allows to
    // find out the concerned DetElement while the second one is the path of
    // a condition inside that element
    auto colonPos   = output.find_first_of( ':' );
    auto de         = m_description.detector( output.substr( 0, colonPos ) );
    auto condKey    = ( colonPos == std::string::npos ? "" : output.substr( colonPos + 1 ) );
    auto dependency = std::make_unique<dd4hep::cond::ConditionDependency>( de, condKey, std::move( func ) );
    for ( auto& input : inputs ) { dependency->dependencies.emplace_back( dd4hep::ConditionKey{makeKey( input )} ); }
    auto r = m_all_conditions->addDependency( dependency.release() );
    return ( r.first = !0 );
  }

} // namespace LHCb::Det::LbDD4hep

DECLARE_COMPONENT( LHCb::Det::LbDD4hep::DD4hepSvc )
