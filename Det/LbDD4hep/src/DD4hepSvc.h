/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LbDD4hep/IDD4hepSvc.h"

#include "Core/DetectorDataService.h"

#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/StatusCode.h"

#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

namespace LHCb::Det::LbDD4hep {

  // Pointer to the Gaudi message service needed by handle_message to forward the messages.
  // handle_message is a normal function as we need to pass a pointer to it to ROOT and DD4hep
  static IMessageSvc* gaudiMessageSvc = nullptr;
  void                handle_message( int level, Bool_t abort, const char* location, const char* msg );

  class DD4hepSvc : public extends<Service, IDD4hepSvc>, Detector::DetectorDataService {

  private:
    Gaudi::Property<std::string>              m_detDescLocation{this, "DescriptionLocation",
                                                   "${DETECTOR_PROJECT_ROOT}/compact/trunk/LHCb.xml",
                                                   "Location of the XML detector description"};
    Gaudi::Property<std::string>              m_conditionsLocation{this, "ConditionsLocation",
                                                      "file://${DETECTOR_PROJECT_ROOT}/ConditionsIOV/",
                                                      "Location of the XML detector conditions"};
    Gaudi::Property<std::vector<std::string>> m_detectorList{
        this, "DetectorList", {"/world", "VP"}, "List of detectors"};

    Gaudi::Property<int> m_verboseLevel{this, "VerboseLevel", 0, "ROOT TGeoManager verbose level"};

  public:
    DD4hepSvc( const std::string& name, ISvcLocator* pSvcLocator )
        : base_class( name, pSvcLocator ), DetectorDataService( dd4hep::Detector::getInstance(), {} ){};

    StatusCode initialize() override;

    StatusCode finalize() override {
      gaudiMessageSvc = nullptr;
      return StatusCode::SUCCESS;
    }

    const dd4hep::Detector& getDetector() const override { return m_description; }

    DD4HepSlicePtr get_slice( size_t iov ) override { return Detector::DetectorDataService::get_slice( iov ); }
    bool add( LHCb::span<const std::string> inputs, const std::string& output, DD4HepDerivationFunc& func ) override;
  };

} // namespace LHCb::Det::LbDD4hep
