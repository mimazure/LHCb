/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
/// GaudiKernel
// ============================================================================
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IDataStoreAgent.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/SmartDataPtr.h"
// ============================================================================
// DetDesc
// ============================================================================
#include "DetDesc/IDetectorElement.h"
#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/IntersectionErrors.h"
#include "DetDesc/Services.h"
#include "DetDesc/VolumeIntersectionIntervals.h"
//#include "DetDesc/Material.h"
// ============================================================================
// ROOT
// ============================================================================
#include "TVector3.h"

// ============================================================================
// local
// ============================================================================
#include "TGeoTransportSvc.h"
// ============================================================================
// Boost
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
/**
 *
 *  implementation of class TGeoTransportSvc
 *
 */
// ============================================================================

// ============================================================================
//  Implementation of initialize() method
// ============================================================================
StatusCode TGeoTransportSvc::initialize() {
  /// initialise the base class
  StatusCode statusCode = Service::initialize();
  if ( statusCode.isFailure() ) return statusCode; // Propagate the error

  m_dataSvc                           = DetDesc::services()->detSvc();
  SmartIF<IDataManagerSvc> dataMgrSvc = DetDesc::services()->detSvc();
  m_dataMgrSvc                        = dataMgrSvc;

  return StatusCode::SUCCESS;
}
// ============================================================================
// Implementation of finalize() method
// ============================================================================
StatusCode TGeoTransportSvc::finalize() { return StatusCode::SUCCESS; }

double TGeoTransportSvc::distanceInRadUnits( const Gaudi::XYZPoint& Point1, const Gaudi::XYZPoint& Point2,
                                             double Threshold, IGeometryInfo* AlternativeGeometry,
                                             IGeometryInfo* GeometryGuess ) const {
  return distanceInRadUnits_r( Point1, Point2, m_accelCache, Threshold, AlternativeGeometry, GeometryGuess );
}

double TGeoTransportSvc::distanceInRadUnits_r( const Gaudi::XYZPoint& point1, const Gaudi::XYZPoint& point2,
                                               std::any& accelCache, double threshold,
                                               IGeometryInfo* alternativeGeometry,
                                               IGeometryInfo* geometryGuess ) const {
  // check for the  distance
  if ( point1 == point2 ) { return 0; }

  // retrieve the history
  const Gaudi::XYZVector Vector( point2 - point1 );

  // initial point on the line
  // direction vector of the line
  // minimal value of the parameter of the line
  // maximal value of the parameter of the line
  // (output) container of intersections
  // threshold value
  // source of the alternative geometry information
  // a guess for navigation
  ILVolume::Intersections local_intersects;
  intersections_r( point1, Vector, 0.0, 1.0, local_intersects, accelCache, threshold, alternativeGeometry,
                   geometryGuess );

  //  radiation length in tick units
  const auto RadLength = std::accumulate( local_intersects.begin(), local_intersects.end(), 0.0,
                                          VolumeIntersectionIntervals::AccumulateIntersections() );

  // scale
  const auto TickLength = std::sqrt( Vector.mag2() );

  return RadLength * TickLength;
}

unsigned long TGeoTransportSvc::intersections( const Gaudi::XYZPoint& point, const Gaudi::XYZVector& vect,
                                               const ISolid::Tick& tickMin, const ISolid::Tick& tickMax,
                                               ILVolume::Intersections& Intercept, double threshold,
                                               IGeometryInfo* alternativeGeometry,
                                               IGeometryInfo* geometryGuess ) const {
  return intersections_r( point, vect, tickMin, tickMax, Intercept, m_accelCache, threshold, alternativeGeometry,
                          geometryGuess );
}

// ============================================================================
/** general method ( returns the "full history" of the volume
 *  boundary intersections
 *  with different material properties between 2 points )
 *  Similar to intersections but with an additional accelerator
 *  cache for local client storage. This method, unlike the above
 *  @see ITransportSvc
 *  @see IGeometryInfo
 *  @see ILVolume
 *  @param Point               initial point on the line
 *  @param Vector              direction vector of the line
 *  @param TickMin             minimal value of line paramater
 *  @param TickMax             maximal value of line parameter
 *  @param Intercept           (output) container of intersections
 *  @param Threshold           threshold value
 *  @param AlternativeGeometry source of alternative geometry information
 *  @param GeometryGuess       a guess for navigation
 */
// ============================================================================
unsigned long TGeoTransportSvc::intersections_r( const Gaudi::XYZPoint& point, const Gaudi::XYZVector& vect,
                                                 const ISolid::Tick& tickMin, const ISolid::Tick& tickMax,
                                                 ILVolume::Intersections& Intercept, std::any& /* accelCache */,
                                                 double /* threshold */, IGeometryInfo* /* alternativeGeometry */,
                                                 IGeometryInfo* /* guessGeometry */ ) const {

  Intercept.clear();

  /// check the input parameters of the line
  if ( tickMin >= tickMax && vect.mag2() <= 0 ) { return 0; }

  Gaudi::XYZPoint start = point + vect * tickMin;
  Gaudi::XYZPoint end   = point + vect * tickMax;

  auto tocm = []( Gaudi::XYZPoint p ) { return Gaudi::XYZPoint( p.X() / 10.0, p.Y() / 10.0, p.Z() / 10.0 ); };
  // Getting the intersections using the ROOT Geometry
  // CAREFUL: The unit is the cm
  Gaudi::XYZPoint             start_cm = tocm( start );
  Gaudi::XYZPoint             end_cm   = tocm( end );
  std::vector<MatIntervalCmp> tgeo_Intercepts;
  getTGeoIntersections( start_cm, end_cm, tgeo_Intercepts, gGeoManager );

  for ( auto mi : tgeo_Intercepts ) {
    /*SmartDataPtr<const Material> material( m_dataSvc, "/dd/Materials/" +
    mi.materialName); if( !material ) { std::cout <<  "Could not locate material
    " <<  mi.materialName << std::endl;
      }*/

    ILVolume::Intersection tmp;
    tmp.first.first  = mi.start;
    tmp.first.second = mi.end;
    tmp.second       = findMaterial( mi.materialName );
    Intercept.push_back( tmp );
  }

  return tgeo_Intercepts.size();
  ///
}

/**
 * We only have as name the last part of the path so we search through the
 * existing materials
 */
const Material* TGeoTransportSvc::findMaterial( std::string shortname ) const {

  auto agent = MaterialsDSAgent( shortname );
  m_dataMgrSvc->traverseSubTree( "/dd/Materials", &agent );

  return agent.m_material;
}

void TGeoTransportSvc::getTGeoIntersections( Gaudi::XYZPoint start, Gaudi::XYZPoint end,
                                             std::vector<MatIntervalCmp>& Intercepts, TGeoManager* m ) const {
  TVector3 v( end.X() - start.X(), end.Y() - start.Y(), end.Z() - start.Z() );

  Double_t xp = v.X() / v.Mag();
  Double_t yp = v.Y() / v.Mag();
  Double_t zp = v.Z() / v.Mag();

  Double_t snext;
  TString  path;
  Double_t pt[3];
  Double_t loc[3];
  Double_t epsil   = 1.E-2;
  Double_t lastrad = 0.;
  Int_t    ismall  = 0;
  Int_t    nbound  = 0;
  Double_t length  = 0.;
  // Double_t safe = 0.;
  Double_t    rad      = 0.;
  TGeoMedium* med      = nullptr;
  TGeoShape*  shape    = nullptr;
  TGeoNode*   lastnode = nullptr;

  m->InitTrack( start.X(), start.Y(), start.Z(), xp, yp, zp );
  if ( this->msgLevel( MSG::DEBUG ) ) {
    debug() << "Track: " << start.X() << ", " << start.Y() << ", " << start.Z() << ", " << xp << ", " << yp << ", "
            << zp << endmsg;
  }
  TGeoNode* nextnode = m->GetCurrentNode();
  while ( nextnode ) {
    med = 0;
    if ( nextnode )
      med = nextnode->GetVolume()->GetMedium();
    else
      return;
    shape    = nextnode->GetVolume()->GetShape();
    lastnode = nextnode;
    nextnode = m->FindNextBoundaryAndStep();
    snext    = m->GetStep();
    if ( snext < 1.e-8 ) {
      ismall++;
      if ( ( ismall < 3 ) && ( lastnode != nextnode ) ) {
        // First try to cross a very thin layer
        length += snext;
        nextnode = m->FindNextBoundaryAndStep();
        snext    = m->GetStep();
        if ( snext < 1.E-8 ) continue;
        // We managed to cross the layer
        ismall = 0;
      } else {
        // Relocate point
        if ( ismall > 3 ) {
          error() << "ERROR: Small steps in: " << m->GetPath() << " shape=" << shape->ClassName() << endmsg;
          return;
        }
        memcpy( pt, m->GetCurrentPoint(), 3 * sizeof( Double_t ) );
        const Double_t* dir = m->GetCurrentDirection();
        for ( Int_t i = 0; i < 3; i++ ) pt[i] += epsil * dir[i];
        snext = epsil;
        length += snext;
        rad += lastrad * snext;
        m->CdTop();
        nextnode = m->FindNode( pt[0], pt[1], pt[2] );
        if ( m->IsOutside() ) return;
        TGeoMatrix* mat = m->GetCurrentMatrix();
        mat->MasterToLocal( pt, loc );
        if ( !m->GetCurrentVolume()->Contains( loc ) ) {
          m->CdUp();
          nextnode = m->GetCurrentNode();
        }
        continue;
      }
    } else {
      ismall = 0;
    }
    nbound++;
    length += snext;
    if ( med ) {
      Double_t radlen = med->GetMaterial()->GetRadLen();
      if ( radlen > 1.e-5 && radlen < 1.e10 ) {
        lastrad = med->GetMaterial()->GetDensity() / radlen;
        rad += lastrad * snext;
      } else {
        lastrad = 0.;
      }

      if ( this->msgLevel( MSG::DEBUG ) ) {
        debug() << "STEP #" << nbound << " " << path.Data() << endmsg;
        debug() << " step=" << snext << " length=" << length
                << " rad=" << med->GetMaterial()->GetDensity() * snext / med->GetMaterial()->GetRadLen()
                << med->GetName() << endmsg;
      }
      // Now filling up the data
      MatIntervalCmp cur;
      cur.start        = ( length - snext ) / v.Mag();
      cur.end          = ( length ) / v.Mag();
      cur.radlength    = med->GetMaterial()->GetDensity() * snext / med->GetMaterial()->GetRadLen();
      cur.material     = med->GetMaterial();
      cur.materialName = med->GetName();
      cur.locations.push_back( m->GetPath() );

      // Checking whether we need to agregate the entries in the vector
      if ( Intercepts.size() > 0 ) {
        MatIntervalCmp& prev = Intercepts.back();
        if ( prev.materialName == cur.materialName ) {
          prev.end = cur.end;
          prev.radlength += cur.radlength;
          prev.locations.push_back( cur.locations.front() );
        } else {
          // Different material, no need to agregate
          Intercepts.push_back( cur );
        }
      } else {
        // first entry in vector anyway
        Intercepts.push_back( cur );
      }
    }
  }
}

// ============================================================================
DECLARE_COMPONENT( TGeoTransportSvc )
// ============================================================================
// The END
// ============================================================================
