
/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#undef NDEBUG
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestWriteToZips
#include "Event/SOACollection.h"
#include "Event/Zip.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"

#include <boost/test/unit_test.hpp>

#include <array>
#include <vector>

// Define a trivial SOACollection-based container with a few columns
struct Int : public LHCb::v2::Event::int_field {};
struct Float : public LHCb::v2::Event::float_field {};
struct VecX : public LHCb::v2::Event::float_field {};
struct VecY : public LHCb::v2::Event::float_field {};
struct VecZ : public LHCb::v2::Event::float_field {};
struct State : public LHCb::v2::Event::float_field {
  enum { XOffset = 0, YOffset, NumColumns };
  template <typename Container>
  [[nodiscard]] static constexpr std::size_t num_columns( Container const& container ) {
    return NumColumns * container.numStates();
  }
  template <typename Container>
  [[nodiscard]] static constexpr std::size_t flat_index( [[maybe_unused]] Container const& container, std::size_t i,
                                                         std::size_t j ) {
    assert( j < NumColumns && i < container.numStates() );
    return i * NumColumns + j;
  }
};

struct Container : public LHCb::v2::Event::SOACollection<Container, Int, Float, VecX, VecY, VecZ, State> {
  Container( int numStates ) : m_numStates{numStates} {}
  [[nodiscard]] std::size_t numStates() const { return m_numStates; }
  // TODO move these methods to SOACollection
  // Add simd_t::size entries to the container and return a proxy object that can be used to write to them
  template <SIMDWrapper::InstructionSet simd>
  auto emplace_back() {
    using simd_t        = SIMDWrapper::type_map_t<simd>;
    auto const old_size = size();
    resize( old_size + simd_t::size );
    return LHCb::Pr::make_zip<simd>( *this )[old_size];
  }
  // TODO could instead take `mask_v` as a template argument and deduce `simd` from it
  // TODO compress_back instead of emplace_back?
  template <SIMDWrapper::InstructionSet simd>
  auto emplace_back( typename SIMDWrapper::type_map_t<simd>::mask_v const& mask ) {
    auto const old_size = size();
    resize( old_size + popcount( mask ) );
    return LHCb::Pr::make_zip<simd>( *this ).compress( old_size, mask );
  }

private:
  int m_numStates{};
  DECLARE_PROXY_FRIEND( Proxy );
};

template <typename ContainerProxy>
struct StateProxy {
  using float_v = decltype( std::declval<ContainerProxy>().stateX( 0 ) );
  StateProxy( ContainerProxy proxy, std::size_t state_index )
      : m_proxy{std::move( proxy )}, m_state_index{state_index} {}
  [[nodiscard]] auto x() const { return m_proxy.stateX( m_state_index ); }
  [[nodiscard]] auto y() const { return m_proxy.stateY( m_state_index ); }
  void               setX( float_v const& x ) const { m_proxy.setStateX( m_state_index, x ); }
  void               setY( float_v const& y ) const { m_proxy.setStateY( m_state_index, y ); }

private:
  ContainerProxy m_proxy;
  std::size_t    m_state_index;
};

// Define a proxy type to access the container
DECLARE_PROXY( Proxy ) {
  PROXY_METHODS( Proxy, simd_t, Container, m_container );
  using int_v   = typename simd_t::int_v;
  using float_v = typename simd_t::float_v;
  // Accessors
  [[nodiscard]] auto i() const { return help<Int>(); }
  [[nodiscard]] auto f() const { return help<Float>(); }
  // It's a matter of convention whether these methods should return values or
  // proxies. One reasonable choice would be that scalar values, vectors and
  // matrices are returned by value, while more complicated objects (e.g.
  // states) are themselves proxies. That is what's implemented here; vec()
  // returns by value and state( i ) returns a proxy.
  [[nodiscard]] auto vec() const { return LHCb::LinAlg::Vec{help<VecX>(), help<VecY>(), help<VecZ>()}; }
  [[nodiscard]] auto stateX( int i ) const { return help<State>( i, State::XOffset ); }
  [[nodiscard]] auto stateY( int i ) const { return help<State>( i, State::YOffset ); }
  // Setters
  void setI( int_v const& i ) const { help_set<Int>( i ); }
  void setF( float_v const& f ) const { help_set<Float>( f ); }
  void setVec( LHCb::LinAlg::Vec<float_v, 3> const& v ) const {
    help_set<VecX>( v( 0 ) );
    help_set<VecY>( v( 1 ) );
    help_set<VecZ>( v( 2 ) );
  }
  void setStateX( int i, float_v const& x ) const { help_set<State>( x, i, State::XOffset ); }
  void setStateY( int i, float_v const& y ) const { help_set<State>( y, i, State::YOffset ); }
  // Return a proxy to the ith state vector
  [[nodiscard]] auto state( std::size_t i ) const {
    assert( i < this->m_container->numStates() );
    return StateProxy{full_proxy().template clone<Container>(), i};
  }

private:
  // Helper specific to `Container`
  template <typename Tag, typename... Ts>
  auto help( Ts... Is ) const {
    return this->load_vector( this->m_container->template data<Tag>( Is... ) );
  }
  template <typename Tag, typename T, typename... Ts>
  void help_set( T const& v, Ts... Is ) const {
    return this->set_vector( this->m_container->template data<Tag>( Is... ), v );
  }
};

REGISTER_PROXY( Container, ::Proxy );

namespace {
  constexpr auto simd = SIMDWrapper::Best;
  using simd_t        = SIMDWrapper::type_map_t<simd>;
  using int_v         = simd_t::int_v;
  using float_v       = simd_t::float_v;
} // namespace

// Test adding space for simd_t::size elements to a container and using a
// writable proxy object to fill in the content.
BOOST_AUTO_TEST_CASE( TestContiguousStore ) {
  Container c{2};
  // Note the use of const, this is to test the intended behaviour that these
  // proxy objects have similar semantics to std::span<T>: a const proxy simply
  // means it cannot be rebound. What determines whether or not the object can
  // be written to is the template argument (std::span<T const> refers to an
  // immutable set of Ts).
  auto const added = c.emplace_back<simd>();
  // Now `added` refers to `simd_t::size` new elements added to the end of `c`
  BOOST_CHECK_EQUAL( c.size(), simd_t::size );
  // The values returned by `added.i()` and `added.f()` are unspecified. It
  // would be nice if the test could assert that...but that seems like it's
  // probably more trouble than it's worth...
  // TODO: should there be a way of telling `emplace_back` that it *should*
  //       initialise the values? if so, we could

  // Check that we can write a value via `added`
  added.setI( int_v{42} );
  // And that we can read back the same value...
  BOOST_CHECK( all( added.i() == 42 ) );
  // Same for the other column
  added.setF( float_v{-7.f} );
  BOOST_CHECK( all( added.f() == -7.f ) );
  // Finally for the vector column
  added.setVec( {1.f, 2.f, 3.f} );
  auto const vec = added.vec();
  BOOST_CHECK( all( added.vec() == decltype( vec ){1.f, 2.f, 3.f} ) );
  // Now try using the state proxies
  auto const state0 = added.state( 0 );
  auto const state1 = added.state( 1 );
  state0.setX( float_v{14.f} );
  state0.setY( float_v{-1.f} );
  state1.setX( float_v{50.f} );
  state1.setY( float_v{-4.f} );
  BOOST_CHECK( all( state0.x() == float_v{14.f} ) );
  BOOST_CHECK( all( state0.y() == float_v{-1.f} ) );
  BOOST_CHECK( all( state1.x() == float_v{50.f} ) );
  BOOST_CHECK( all( state1.y() == float_v{-4.f} ) );
  BOOST_CHECK( all( added.stateX( 0 ) == state0.x() ) );
  BOOST_CHECK( all( added.stateY( 0 ) == state0.y() ) );
  BOOST_CHECK( all( added.stateX( 1 ) == state1.x() ) );
  BOOST_CHECK( all( added.stateY( 1 ) == state1.y() ) );
}

// Test adding space for `popcount( mask )` elements to a container and using a
// writable proxy object to fill in the content using compress-store operations
BOOST_AUTO_TEST_CASE( TestCompressStore ) {
  Container c{1};
  // Construct a mask that alternates {0101...}
  auto const mask = [] {
    std::array<int, simd_t::size> tmp;
    for ( auto i = 0ul; i < tmp.size(); ++i ) { tmp[i] = i % 2; }
    auto const mask = int_v{tmp.data()} > int_v{0};
    for ( auto i = 0ul; i < tmp.size(); ++i ) { BOOST_CHECK_EQUAL( testbit( mask, i ), tmp[i] ); }
    return mask;
  }();
  BOOST_CHECK_EQUAL( popcount( mask ), simd_t::size / 2 );
  // Construct a writable proxy object that can be used to compress-store
  // values using `mask`
  auto const added = c.emplace_back<simd>( mask );
  BOOST_CHECK_EQUAL( c.size(), popcount( mask ) );
  // Store {0, 1, 2, ...} compressed with {010101...},
  // for 256bit this should lead to {1, 3, 5, 7, junk, junk, junk, junk} being
  // stored...note that this is dangerous if the proxy does not refer to the
  // end of the container, where SOACollection guarantees the junk writes are
  // safe. TODO throw/warn in that case
  added.setI( simd_t::indices() );
  added.setF( simd_t::indices() );
  added.setVec( {simd_t::indices(), simd_t::indices(), simd_t::indices()} );
  // Read the values back...compress proxies are write-only [for the moment],
  // so instead let's make a contiguous read proxy into the container and use
  // that for the checking.
  auto const contiguous_added = LHCb::Pr::make_zip<simd>( c )[0];
  BOOST_CHECK_EQUAL( popcount( contiguous_added.loop_mask() ), popcount( mask ) );
  // Helper to generate {1, 3, 5, ..., junk * simd_t::size / 2}
  auto const compressed = [] {
    std::array<int, simd_t::size> tmp;
    for ( auto i = 0ul; i < tmp.size(); ++i ) { tmp[i] = i < tmp.size() / 2 ? 2 * i + 1 : -42; }
    return int_v{tmp.data()};
  }();
  // Construct a mask {11...0...} (half ones, then half zeroes), these are the
  // entries in `added.i()` that should be valid
  auto const compressed_mask = simd_t::loop_mask( simd_t::size / 2, simd_t::size );
  BOOST_CHECK( none( !compressed_mask && contiguous_added.loop_mask() ) );
  // Check that the stored values are as expected
  BOOST_CHECK( all( contiguous_added.i() == compressed || !compressed_mask ) );
  BOOST_CHECK( all( contiguous_added.f() == compressed || !compressed_mask ) );
  auto const compressed_vec = contiguous_added.vec();
  using vec_t               = decltype( compressed_vec );
  BOOST_CHECK( all( compressed_vec == vec_t{compressed, compressed, compressed} || !compressed_mask ) );
}

// Define a new type that has a variable number of indices per row. For example
// this could be a track container that stores indices to a variable number of
// hits per track.
namespace WithIndices {
  // Tag types for fields in the "track container"
  struct IndicesBegin : public LHCb::v2::Event::int_field {};
  struct IndicesEnd : public LHCb::v2::Event::int_field {};
  // helpers to avoid writing out the lists of tags twice
  template <typename Base>
  using MakeMainBase = LHCb::v2::Event::SOACollection<Base, IndicesBegin, IndicesEnd>;
  template <typename Base>
  using MakeHitIndicesBase = LHCb::v2::Event::SOACollection<Base, Int>;
  // define the "track container"
  struct Container : public MakeMainBase<Container>, public MakeHitIndicesBase<Container> {
    // this container has two different SOACollection-derived base classes, so
    // we need to explicitly specify which one the methods like `size()` come
    // from...define some shorthand for the two base classes
    using MainBase       = MakeMainBase<Container>;
    using HitIndicesBase = MakeHitIndicesBase<Container>;
    Container()          = default;
    Container( Zipping::ZipFamilyNumber zn, Container const& old ) : MainBase( zn, old ), HitIndicesBase( zn, old ) {}
    // These are defined in both classes, but the ones we want are in `MainBase`
    using MainBase::resize;
    using MainBase::size;
    using MainBase::zipIdentifier;
    // Methods for accessing the hit indices from the "second" SOACollection
    // HitIndicesBase::zipIdentifier exists, but doesn't make sense semantically
    void                      indices_resize( std::size_t size ) { HitIndicesBase::resize( size ); }
    void                      indices_reserve( std::size_t capacity ) { HitIndicesBase::reserve( capacity ); }
    [[nodiscard]] std::size_t indices_size() const { return HitIndicesBase::size(); }
    [[nodiscard]] int*        indices() { return HitIndicesBase::data<Int>(); }
    [[nodiscard]] int const*  indices() const { return HitIndicesBase::data<Int>(); }
    // Selectively copying this container is not entirely trivial, so we need
    // to put in some logic explicitly.
    template <typename simd_t>
    void copy_back( Container const& from, int at ) {
      // Unconditionally copy `simd_t::size` elements starting at offset `at`
      // in `from` into the end of `this`. First, let the base class do this
      // for us for the "track" columns...in principle this would Just Work for
      // any track parameter columns. In practice, here we only have the two
      // index columns, and this does **not** do the right thing with those.
      MainBase::copy_back<simd_t>( from, at );
      // Now the back simd_t::size elements of `this` are populated with
      // begin/end indices into `from.indices()`, and `this.indices()` has not
      // been updated yet. Get a proxy object for those added elements.
      auto const added = LHCb::Pr::make_zip<simd_t::instructionSet()>( *this )[size() - simd_t::size];
      // What's the full range of indices into `from.indices()` we need to copy
      // Note this assumes that the old container `from` had the "obvious"
      // compact structure. If it didn't then this would be suboptimal.
      auto const old_indices_begin    = added.hitIndicesBegin().hmin();
      auto const old_indices_end      = added.hitIndicesEnd().hmax();
      auto const new_indices_old_size = indices_size();
      for ( auto old_indices_i = old_indices_begin; old_indices_i < old_indices_end; old_indices_i += simd_t::size ) {
        HitIndicesBase::copy_back<simd_t>( from, old_indices_i, simd_t::loop_mask( old_indices_i, old_indices_end ) );
      }
      // Update the begins/ends in `added`
      added.setHitIndicesBegin( added.hitIndicesBegin() - old_indices_begin + new_indices_old_size );
      added.setHitIndicesEnd( added.hitIndicesEnd() - old_indices_begin + new_indices_old_size );
    }
    template <typename simd_t>
    void copy_back( Container const& from, int at, typename simd_t::mask_v const& mask ) {
      // Selectively copy the "track" columns (begin/end indices, in this case)
      MainBase::copy_back<simd_t>( from, at, mask );
      // Get a proxy object for reading the values we just copied...they have
      // been compressed, so `popcount( mask )` elements were added to `this`
      auto const added = LHCb::Pr::make_zip<simd_t::instructionSet()>( *this )[size() - popcount( mask )];
      // Now the first `popcount( mask )` elements of `added` are valid. A mask
      // corresponding to that is given by `added.loop_mask()`.
      BOOST_CHECK_EQUAL( popcount( added.loop_mask() ), popcount( mask ) );
      // Now we need to copy over the indices that are referred to by retained
      // "tracks" without copying the indices of rejected "tracks".
      std::array<int, simd_t::size> indices_begin, indices_end;
      added.hitIndicesBegin().store( indices_begin.data() );
      added.hitIndicesEnd().store( indices_end.data() );
      auto new_indices_size = indices_size();
      for ( auto added_i = 0; added_i < popcount( mask ); ++added_i ) {
        for ( auto old_indices_i = indices_begin[added_i]; old_indices_i < indices_end[added_i];
              old_indices_i += simd_t::size ) {
          HitIndicesBase::copy_back<simd_t>( from, old_indices_i,
                                             simd_t::loop_mask( old_indices_i, indices_end[added_i] ) );
        }
        // update `indices_begin` and `indices_end` so they can be stored in `added`
        auto const num_indices = indices_end[added_i] - indices_begin[added_i];
        indices_begin[added_i] = new_indices_size;
        indices_end[added_i]   = new_indices_size + num_indices;
        new_indices_size += num_indices;
      }
      // Update the begins/ends in `added`
      using int_v = typename simd_t::int_v;
      added.setHitIndicesBegin( int_v{indices_begin} );
      added.setHitIndicesEnd( int_v{indices_end} );
    }
    // TODO: The emplace_back methods below should be part of SOACollection (I guess)
    // Add simd_t::size entries to the container and return a proxy object that can be used to write to them
    template <SIMDWrapper::InstructionSet simd>
    auto emplace_back() {
      using simd_t        = SIMDWrapper::type_map_t<simd>;
      auto const old_size = size();
      resize( old_size + simd_t::size );
      return LHCb::Pr::make_zip<simd>( *this )[old_size];
    }
    // TODO could instead take `mask_v` as a template argument and deduce `simd` from it
    template <SIMDWrapper::InstructionSet simd>
    auto emplace_back( typename SIMDWrapper::type_map_t<simd>::mask_v const& mask ) {
      auto const old_size = size();
      resize( old_size + popcount( mask ) );
      return LHCb::Pr::make_zip<simd>( *this ).compress( old_size, mask );
    }

  private:
    DECLARE_PROXY_FRIEND( Proxy );
  };
  DECLARE_PROXY( Proxy ) {
    PROXY_METHODS( Proxy, simd_t, Container, m_container );
    using int_v   = typename simd_t::int_v;
    using float_v = typename simd_t::float_v;
    [[nodiscard]] auto hitIndices() const {
      static_assert( simd_t::size == 1, "hitIndices() only works for scalar proxies" );
      auto const* indices_data = this->m_container->indices();
      return LHCb::span{std::next( indices_data, hitIndicesBegin().cast() ),
                        std::next( indices_data, hitIndicesEnd().cast() )};
    }
    [[nodiscard]] auto hitIndicesBegin() const { return help<IndicesBegin>(); }
    [[nodiscard]] auto hitIndicesEnd() const { return help<IndicesEnd>(); }
    [[nodiscard]] auto nHits() const { return hitIndicesEnd() - hitIndicesBegin(); }
    void               setHitIndicesBegin( int_v const& begin ) const { help_set<IndicesBegin>( begin ); }
    void               setHitIndicesEnd( int_v const& end ) const { help_set<IndicesEnd>( end ); }

  private:
    // Helper specific to `Container`
    template <typename Tag>
    auto help() const {
      return this->load_vector( this->m_container->Container::MainBase::template data<Tag>() );
    }
    template <typename Tag, typename T>
    void help_set( T const& v ) const {
      return this->set_vector( this->m_container->Container::MainBase::template data<Tag>(), v );
    }
  };
} // namespace WithIndices
REGISTER_PROXY( WithIndices::Container, WithIndices::Proxy );

BOOST_AUTO_TEST_CASE( StoreWithVariableIndices ) {
  WithIndices::Container c{};
  // Now `added` refers to `simd_t::size` new elements added to the end of `c`
  auto const added = c.emplace_back<simd>();
  BOOST_CHECK_EQUAL( c.size(), simd_t::size );
  std::array<int, simd_t::size + 1> begin_indices{}; // zero-initialise
  for ( auto i = 1ul; i < begin_indices.size(); ++i ) {
    auto const hits_to_add  = i * i; // Nth track we add has (N + 1)^2 hits
    auto const old_num_hits = c.indices_size();
    c.indices_resize( old_num_hits + hits_to_add );
    for ( auto nhit = 0ul; nhit < hits_to_add; ++nhit ) {
      // make up some hit indices..Nth track has indices from (N + 1)^3 .. (N + 1)^3 + (N + 1)^2 - 1
      c.indices()[old_num_hits + nhit] = ( i * i * i ) + nhit;
    }
    begin_indices[i] = begin_indices[i - 1] + hits_to_add;
  }
  // Store the indices to hit ranges that we just filled in
  // Here we are explicitly saving columns for begin/end of the range, even
  // though we know this contains redundant information.
  added.setHitIndicesBegin( int_v{begin_indices.data()} );
  added.setHitIndicesEnd( int_v{std::next( begin_indices.data() )} );
  // Check it all worked...first look at the numbers of hits
  std::size_t                   total_hits{};
  std::array<int, simd_t::size> expected_nhits;
  for ( auto i = 0ul; i < expected_nhits.size(); ++i ) { expected_nhits[i] = std::pow( i + 1, 2 ); }
  BOOST_CHECK( all( added.nHits() == int_v{expected_nhits.data()} ) );
  // Now check the indices themselves. Easiest to do this with a scalar loop
  for ( auto i = 0ul; i < c.size(); ++i ) {
    auto const track       = LHCb::Pr::make_zip<SIMDWrapper::Scalar>( c )[i];
    auto const hit_indices = track.hitIndices();
    total_hits += hit_indices.size();
    // Check again we have the right number of hits
    BOOST_CHECK_EQUAL( hit_indices.size(), std::pow( i + 1, 2 ) );
    // Check the actual values are the same ones we made up above
    for ( auto nhit = 0ul; nhit < hit_indices.size(); ++nhit ) {
      BOOST_CHECK_EQUAL( hit_indices[nhit], std::pow( i + 1, 3 ) + nhit );
    }
  }
  BOOST_CHECK_EQUAL( total_hits, c.indices_size() );
  // Now test copying this container. This should keep the elements of `c` with
  // an odd number of hits (the even entries)
  auto const only_even =
      LHCb::Pr::make_zip<simd>( c ).filter( []( auto const& track ) { return ( track.nHits() & 1 ) == 1; } );
  static_assert( simd_t::size % 2 == 0 );
  BOOST_CHECK_EQUAL( only_even.size(), simd_t::size / 2 );
  // Get a proxy to the first simd_t::size elements in the new container.
  auto const retained = LHCb::Pr::make_zip<simd>( only_even )[0];
  // Note that only the first half of these elements are actually valid...check
  // that.
  BOOST_CHECK( none( !retained.loop_mask() && simd_t::loop_mask( simd_t::size / 2, simd_t::size ) ) );
  // Check the content with a scalar loop
  std::size_t total_even_hits{};
  for ( auto i_new = 0ul; i_new < only_even.size(); ++i_new ) {
    auto const track       = LHCb::Pr::make_zip<SIMDWrapper::Scalar>( only_even )[i_new];
    auto const hit_indices = track.hitIndices();
    total_even_hits += hit_indices.size();
    // there's a factor of 2 here w.r.t. the `i` in the loop above
    auto const i_old = i_new * 2;
    BOOST_CHECK_EQUAL( hit_indices.size(), std::pow( i_old + 1, 2 ) );
    for ( auto nhit = 0ul; nhit < hit_indices.size(); ++nhit ) {
      BOOST_CHECK_EQUAL( hit_indices[nhit], std::pow( i_old + 1, 3 ) + nhit );
    }
  }
  BOOST_CHECK_EQUAL( total_even_hits, only_even.indices_size() );
}