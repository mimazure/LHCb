/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedCaloHypo.h"

#include "GaudiAlg/Producer.h"

/**
 * Packer for CaloHypo
 *
 * FIXME : despite this looks functional, the input is still handled the old way and
 * retrieved from the TES via getOrCreate. This should be fixed but requires that
 * the algorithm generating the input is functional itself
 */
struct PackCaloHypo : Gaudi::Functional::Producer<LHCb::PackedCaloHypos()> {

  PackCaloHypo( const std::string& name, ISvcLocator* pSvcLocator )
      : Producer( name, pSvcLocator, KeyValue{"OutputName", LHCb::PackedCaloHypoLocation::Electrons} ) {}

  LHCb::PackedCaloHypos operator()() const override;

  Gaudi::Property<std::string> m_inputName{this, "InputName", LHCb::CaloHypoLocation::Electrons};
  Gaudi::Property<bool>        m_enableCheck{this, "EnableCheck", false,
                                      "Flag to turn on automatic unpacking and checking of the output post-packing"};
};

LHCb::PackedCaloHypos PackCaloHypo::operator()() const {

  LHCb::CaloHypos* hypos = getOrCreate<LHCb::CaloHypos, LHCb::CaloHypos>( m_inputName );
  if ( !hypos ) return {};

  // create ouput
  LHCb::PackedCaloHypos out;
  out.setVersion( 1 );
  out.setPackingVersion( LHCb::PackedCaloHypos::defaultPackingVersion() );

  // pack
  const LHCb::CaloHypoPacker packer( this );
  packer.pack( *hypos, out );

  // Packing checks
  if ( UNLIKELY( m_enableCheck ) ) {
    // make new unpacked output data object
    LHCb::CaloHypos unpacked;
    // unpack
    packer.unpack( out, unpacked );
    // run checks
    packer.check( *hypos, unpacked ).ignore();
  }

  return out;
}

DECLARE_COMPONENT( PackCaloHypo )
