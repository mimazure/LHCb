/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedTrack.h"
#include "Event/StandardPacker.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PackTrack
//
// 2008-11-12 : Olivier Callot
//-----------------------------------------------------------------------------

/** @class PackTrack PackTrack.h
 *
 *  Pack a track container
 *
 *  @author Olivier Callot
 *  @date   2008-11-12
 */
class PackTrack : public GaudiAlgorithm {

public:
  /// Standard constructor
  PackTrack( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  std::string m_inputName;    ///< Input location
  std::string m_outputName;   ///< Output location
  bool        m_alwaysOutput; ///< Flag to turn on the creation of output, even when input is missing
  bool        m_deleteInput;  ///< Flag to turn on the removal of the input data after packing
  bool        m_enableCheck;  ///< Flag to turn on automatic unpacking and checking of the output post-packing

  Gaudi::Accumulators::StatCounter<> m_nbPackedTracks{this, "# PackedTracks"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PackTrack )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PackTrack::PackTrack( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "InputName", m_inputName = LHCb::TrackLocation::Default );
  declareProperty( "OutputName", m_outputName = LHCb::PackedTrackLocation::Default );
  declareProperty( "AlwaysCreateOutput", m_alwaysOutput = false );
  declareProperty( "DeleteInput", m_deleteInput = false );
  declareProperty( "EnableCheck", m_enableCheck = false );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PackTrack::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  // If input does not exist, and we aren't making the output regardless, just return
  if ( !m_alwaysOutput && !exist<LHCb::Tracks>( m_inputName ) ) return StatusCode::SUCCESS;

  // Input
  LHCb::Tracks* tracks = getOrCreate<LHCb::Tracks, LHCb::Tracks>( m_inputName );

  // Output
  LHCb::PackedTracks* out = new LHCb::PackedTracks();
  put( out, m_outputName );
  out->setVersion( 5 );

  // Track Packer
  const LHCb::TrackPacker packer( this );

  // Pack the tracks
  packer.pack( *tracks, *out );

  // Packing checks
  if ( UNLIKELY( m_enableCheck ) ) {
    // make new unpacked output data object
    LHCb::Tracks* unpacked = new LHCb::Tracks();
    put( unpacked, m_inputName + "_PackingCheck" );

    // unpack
    packer.unpack( *out, *unpacked );

    // run checks
    packer.check( *tracks, *unpacked ).ignore();

    // clean up after checks
    StatusCode sc = evtSvc()->unregisterObject( unpacked ).andThen( [&] { delete unpacked; } );
    if ( sc.isFailure() ) return sc;
  }

  // If requested, remove the input data from the TES and delete
  if ( UNLIKELY( m_deleteInput ) ) {
    const StatusCode sc = evtSvc()->unregisterObject( tracks ).andThen( [&] {
      delete tracks;
      tracks = nullptr;
    } );
    if ( sc.isFailure() ) return sc;
  } else {
    // Clear the registry address of the unpacked container, to prevent reloading
    auto* pReg = tracks->registry();
    if ( pReg ) pReg->setAddress( nullptr );
  }

  // Summary of the size of the PackTracks container
  m_nbPackedTracks += out->tracks().size();

  return StatusCode::SUCCESS;
}

//=============================================================================
