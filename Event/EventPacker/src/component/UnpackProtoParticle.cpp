/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
// Implementation file for class : UnpackProtoParticle
//
// 2008-11-14 : Olivier Callot
//-----------------------------------------------------------------------------
#include "Event/PackedProtoParticle.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Interfaces/IProtoParticleTool.h"

/** @brief Unpack a PackedProtoParticle container to ProtoParticles.
 *
 *  @author Olivier Callot
 *  @date   2008-11-14
 */
class UnpackProtoParticle : public GaudiAlgorithm {

public:
  using GaudiAlgorithm::GaudiAlgorithm;
  StatusCode execute() override;

private:
  DataObjectReadHandle<LHCb::PackedProtoParticles> m_packedProtos{this, "InputName",
                                                                  LHCb::PackedProtoParticleLocation::Charged};
  DataObjectWriteHandle<LHCb::ProtoParticles>      m_protos{this, "OutputName", LHCb::ProtoParticleLocation::Charged};
  Gaudi::Property<bool>                            m_alwaysOutput{this, "AlwaysCreateOutput", false,
                                       "Flag to turn on the creation of output, even when input is missing"};
  ToolHandleArray<LHCb::Rec::Interfaces::IProtoParticles> m_addInfo{this, "AddInfo", {}};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( UnpackProtoParticle )

StatusCode UnpackProtoParticle::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  // If input does not exist, and we aren't making the output regardless, just return
  if ( !m_alwaysOutput.value() && !m_packedProtos.exist() ) return StatusCode::SUCCESS;

  const auto* dst = m_packedProtos.getOrCreate();
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Found " << dst->protos().size() << " PackedProtoParticles at " << m_packedProtos.fullKey() << endmsg;

  // NOTE: the output container _must be on the TES_ prior to passing it to the
  //      unpacker, as otherwise filling the references to other objects does
  //      not work
  auto* newProtoParticles = m_protos.put( std::make_unique<LHCb::ProtoParticles>() );

  // unpack
  LHCb::ProtoParticlePacker{this}.unpack( *dst, *newProtoParticles );

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Created " << newProtoParticles->size() << " ProtoParticles at " << m_protos.fullKey() << endmsg;

  for ( auto& addInfo : m_addInfo ) ( *addInfo )( *newProtoParticles ).ignore();

  return StatusCode::SUCCESS;
}

//=============================================================================
