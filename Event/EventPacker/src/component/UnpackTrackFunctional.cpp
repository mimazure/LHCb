/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedTrack.h"
#include "Event/StandardPacker.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UnpackTrackFunctional
//
// 2008-11-14 : Olivier Callot
//-----------------------------------------------------------------------------

/** @class UnpackTrackFunctional UnpackTrackFunctional.h
 *
 *  Unpack the PackedTrack
 *
 *  @author Olivier Callot
 *  @date   2008-11-14
 */
class UnpackTrackFunctional final : public Gaudi::Functional::Transformer<LHCb::Tracks( const LHCb::PackedTracks& )> {

public:
  /// Standard constructor
  UnpackTrackFunctional( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, KeyValue{"InputName", LHCb::PackedTrackLocation::Default},
                     KeyValue{"OutputName", LHCb::TrackLocation::Default} ) {}

  /// Functional operator
  LHCb::Tracks operator()( const LHCb::PackedTracks& in ) const override {
    LHCb::Tracks out;
    LHCb::TrackPacker{this}.unpack( in, out );
    m_unpackedTracks += out.size();
    return out;
  }

private:
  mutable Gaudi::Accumulators::AveragingCounter<unsigned long> m_unpackedTracks{this, "# Unpacked Tracks"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( UnpackTrackFunctional )
