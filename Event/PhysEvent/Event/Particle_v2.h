/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PrFittedForwardTracks.h"
#include "Event/PrMuonPIDs.h"
#include "Event/SOACollection.h"
#include "Event/Zip.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/ParticleID.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/Variant.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"

#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"

#include <array>
#include <limits>
#include <tuple>
#include <type_traits>
#include <vector>

namespace LHCb::v2 {
  namespace details {
    constexpr int popcount( bool x ) { return x; }
  } // namespace details

  namespace constants {
    constexpr bool max_calo_hypos = 100;
  } // namespace constants

  struct RichPIDs {
    RichPIDs( Zipping::ZipFamilyNumber zn = Zipping::generateZipIdentifier() ) : zip_identifier( std::move( zn ) ) {}
    // need this one for zipping
    RichPIDs( Zipping::ZipFamilyNumber zn, RichPIDs const& /*old*/ ) : zip_identifier( std::move( zn ) ) {}
    RichPIDs( RichPIDs const& ) = delete;
    RichPIDs( RichPIDs&& )      = default;
    template <typename dType, typename Mask>
    void copy_back( RichPIDs const& from, int at, Mask mask ) {
      using I = typename dType::int_v;
      // How many elements are we adding?
      using details::popcount;
      auto to_add = popcount( mask );

      // How many elements do we already have?
      auto old_size = size();

      // Make sure there's space for the incoming values
      reserve( old_size + to_add + dType::size );
      resize( old_size + to_add );

      // Do the copy
      I( &from.m_pid_codes[at] ).compressstore( mask, m_pid_codes.data() + old_size );
    }

    static int invalid_entry() { return std::numeric_limits<int>::lowest(); };

    auto        zipIdentifier() const { return zip_identifier; }
    std::size_t size() const { return m_pid_codes.size(); };
    template <typename dType>
    auto PIDCode( size_t offset ) const {
      return typename dType::int_v( &m_pid_codes[offset] );
    }

    void reserve( std::size_t size ) { m_pid_codes.reserve( LHCb::v2::Event::safe_simd_capacity<int>( size ) ); }

    void resize( std::size_t size ) {
      m_pid_codes.resize( size ); // TODO switch out for something faster
    }

    // members
    Zipping::ZipFamilyNumber zip_identifier;
    std::vector<int>         m_pid_codes{};
  };

  DECLARE_PROXY( RichPIDProxy ) {
    PROXY_METHODS( RichPIDProxy, dType, RichPIDs, m_pids );
    auto pid_code() const { return this->load_vector( this->m_pids->m_pid_codes.data() ); }
  };

  struct AssignedMasses {
    AssignedMasses( Zipping::ZipFamilyNumber zn = Zipping::generateZipIdentifier() )
        : zip_identifier( std::move( zn ) ) {}
    // need this one for zipping
    AssignedMasses( Zipping::ZipFamilyNumber zn, AssignedMasses const& /*old*/ ) : zip_identifier( std::move( zn ) ) {}
    AssignedMasses( AssignedMasses const& ) = delete;
    AssignedMasses( AssignedMasses&& )      = default;
    template <typename dType, typename Mask>
    void copy_back( AssignedMasses const& from, int at, Mask mask ) {
      using F = typename dType::float_v;
      // How many elements are we adding?
      using details::popcount;
      auto to_add = popcount( mask );

      // How many elements do we already have?
      auto old_size = size();

      // Make sure there's space for the incoming values
      reserve( old_size + to_add + dType::size );
      resize( old_size + to_add );

      // Do the copy
      F( &from.m_masses[at] ).compressstore( mask, m_masses.data() + old_size );
    }

    static float invalid_entry() { return std::numeric_limits<float>::lowest(); };

    auto        zipIdentifier() const { return zip_identifier; }
    std::size_t size() const { return m_masses.size(); };
    template <typename dType>
    auto mass( size_t offset ) const {
      return typename dType::float_v( &m_masses[offset] );
    }

    void reserve( std::size_t size ) { m_masses.reserve( LHCb::v2::Event::safe_simd_capacity<float>( size ) ); }

    void resize( std::size_t size ) { m_masses.resize( size ); }

    // members
    Zipping::ZipFamilyNumber zip_identifier;
    std::vector<float>       m_masses{};
  };

  DECLARE_PROXY( AssignedMassProxy ) {
    PROXY_METHODS( AssignedMassProxy, dType, AssignedMasses, m_masses );
    auto mass() const { return this->load_vector( this->m_masses->m_masses.data() ); }
  };

  struct ParticleIDs { // TODO make this correct an actual particleid
    ParticleIDs( Zipping::ZipFamilyNumber zn = Zipping::generateZipIdentifier() ) : zip_identifier( std::move( zn ) ) {}
    // need this one for zipping
    ParticleIDs( Zipping::ZipFamilyNumber zn, ParticleIDs const& /*old*/ ) : zip_identifier( std::move( zn ) ) {}
    ParticleIDs( ParticleIDs const& ) = delete;
    ParticleIDs( ParticleIDs&& )      = default;
    template <typename dType, typename Mask>
    void copy_back( ParticleIDs const& from, int at, Mask mask ) {
      using I = typename dType::int_v;
      // How many elements are we adding?
      using details::popcount;
      auto to_add = popcount( mask );

      // How many elements do we already have?
      auto old_size = size();

      // Make sure there's space for the incoming values
      reserve( old_size + to_add + dType::size );
      resize( old_size + to_add );

      // Do the copy
      I( &from.m_ids[at] ).compressstore( mask, m_ids.data() + old_size );
    }

    static int invalid_entry() { return std::numeric_limits<int>::lowest(); }

    auto        zipIdentifier() const { return zip_identifier; }
    std::size_t size() const { return m_ids.size(); };

    template <typename dType>
    auto pid( size_t offset ) const {
      return typename dType::int_v( &m_ids[offset] );
    }

    void reserve( std::size_t size ) { m_ids.reserve( LHCb::v2::Event::safe_simd_capacity<int>( size ) ); }
    void resize( std::size_t size ) { m_ids.resize( size ); }

    // members
    Zipping::ZipFamilyNumber zip_identifier;
    std::vector<int>         m_ids{};
  };

  DECLARE_PROXY( ParticleIDProxy ) {
    PROXY_METHODS( ParticleIDProxy, dType, ParticleIDs, m_pids );
    auto pid() const { return this->load_vector( this->m_pids->m_ids.data() ); }
  };

  struct CombDLLs {
    CombDLLs( Zipping::ZipFamilyNumber zn = Zipping::generateZipIdentifier() ) : zip_identifier( std::move( zn ) ) {}
    // need this one for zipping
    CombDLLs( Zipping::ZipFamilyNumber zn, CombDLLs const& /*old*/ ) : zip_identifier( std::move( zn ) ) {}
    CombDLLs( CombDLLs const& ) = delete;
    CombDLLs( CombDLLs&& )      = default;
    template <typename dType, typename Mask>
    void copy_back( CombDLLs const& from, int at, Mask mask ) {
      using F = typename dType::float_v;
      // How many elements are we adding?
      using details::popcount;
      auto to_add = popcount( mask );

      // How many elements do we already have?
      auto old_size = size();

      // Make sure there's space for the incoming values
      reserve( old_size + to_add + dType::size );
      resize( old_size + to_add );

      // Do the copy
      F( &from.m_CombDLLps[at] ).compressstore( mask, m_CombDLLps.data() + old_size );
      F( &from.m_CombDLLes[at] ).compressstore( mask, m_CombDLLes.data() + old_size );
      F( &from.m_CombDLLpis[at] ).compressstore( mask, m_CombDLLpis.data() + old_size );
      F( &from.m_CombDLLks[at] ).compressstore( mask, m_CombDLLks.data() + old_size );
      F( &from.m_CombDLLmus[at] ).compressstore( mask, m_CombDLLmus.data() + old_size );
    }

    static float invalid_entry() { return std::numeric_limits<float>::lowest(); };

    auto        zipIdentifier() const { return zip_identifier; }
    std::size_t size() const { return m_CombDLLps.size(); };
    template <typename dType>
    auto CombDLLp( size_t offset ) const {
      return typename dType::float_v( &m_CombDLLps[offset] );
    }
    template <typename dType>
    auto CombDLLe( size_t offset ) const {
      return typename dType::float_v( &m_CombDLLes[offset] );
    }
    template <typename dType>
    auto CombDLLpi( size_t offset ) const {
      return typename dType::float_v( &m_CombDLLpis[offset] );
    }
    template <typename dType>
    auto CombDLLk( size_t offset ) const {
      return typename dType::float_v( &m_CombDLLks[offset] );
    }
    template <typename dType>
    auto CombDLLmu( size_t offset ) const {
      return typename dType::float_v( &m_CombDLLmus[offset] );
    }

    void reserve( std::size_t size ) {
      auto const safe_capacity = LHCb::v2::Event::safe_simd_capacity<float>( size );
      m_CombDLLps.reserve( safe_capacity );
      m_CombDLLes.reserve( safe_capacity );
      m_CombDLLpis.reserve( safe_capacity );
      m_CombDLLks.reserve( safe_capacity );
      m_CombDLLmus.reserve( safe_capacity );
    }

    void resize( std::size_t size ) {
      m_CombDLLps.resize( size );
      m_CombDLLes.resize( size );
      m_CombDLLpis.resize( size );
      m_CombDLLks.resize( size );
      m_CombDLLmus.resize( size );
    }

    // members
    Zipping::ZipFamilyNumber zip_identifier;
    std::vector<float>       m_CombDLLps{};
    std::vector<float>       m_CombDLLes{};
    std::vector<float>       m_CombDLLpis{};
    std::vector<float>       m_CombDLLks{};
    std::vector<float>       m_CombDLLmus{};
  };

  DECLARE_PROXY( CombDLLProxy ) {
    PROXY_METHODS( CombDLLProxy, dType, CombDLLs, m_CombDLLs );
    auto CombDLLp() const { return this->load_vector( this->m_CombDLLs->m_CombDLLps.data() ); }
    auto CombDLLe() const { return this->load_vector( this->m_CombDLLs->m_CombDLLes.data() ); }
    auto CombDLLpi() const { return this->load_vector( this->m_CombDLLs->m_CombDLLpis.data() ); }
    auto CombDLLk() const { return this->load_vector( this->m_CombDLLs->m_CombDLLks.data() ); }
    auto CombDLLmu() const { return this->load_vector( this->m_CombDLLs->m_CombDLLmus.data() ); }
  };

  namespace CompositeTags {
    // Constants defining array/matrix members
    inline constexpr std::size_t NumPositionEntries    = 3;
    inline constexpr std::size_t NumPositionCovEntries = NumPositionEntries * ( NumPositionEntries + 1 ) / 2;
    inline constexpr std::size_t NumFourMomEntries     = 4;
    inline constexpr std::size_t NumFourMomCovEntries  = NumFourMomEntries * ( NumFourMomEntries + 1 ) / 2;

    // Tag types
    struct PID : public Event::int_field {};
    struct nDoF : public Event::int_field {};
    struct Chi2 : public Event::float_field {};
    /** Child relations -- two int columns per child.
     */
    struct ChildRels : public Event::int_field {
      enum { IndexOffset = 0, ZipFamilyOffset, NumColumns };
      template <typename T>
      static constexpr std::size_t num_columns( T const& composites ) {
        return NumColumns * composites.numChildren();
      }
      template <typename T>
      [[nodiscard]] static constexpr std::size_t flat_index( [[maybe_unused]] T const& composites, std::size_t child,
                                                             std::size_t column ) {
        assert( column < NumColumns && child < composites.numChildren() );
        return NumColumns * child + column;
      }
    };
    // Position vector (3D)
    struct Position : public Event::floats_field<NumPositionEntries> {};
    // Position covariance (3D symmetric)
    struct PositionCov : public Event::floats_field<NumPositionCovEntries> {};
    // Four-momentum (4D...)
    struct FourMom : public Event::floats_field<NumFourMomEntries> {};
    // Four-momentum covariance (4D symmetric)
    struct FourMomCov : public Event::floats_field<NumFourMomCovEntries> {};
    // Position-four-momentum covariance (4x3)
    struct PositionFourMomCov : public Event::floats_field<NumFourMomEntries, NumPositionEntries> {};

    template <typename T>
    using base_t = Event::SOACollection<T, PID, nDoF, Chi2, Position, PositionCov, FourMom, FourMomCov,
                                        PositionFourMomCov, ChildRels>;
  } // namespace CompositeTags

  struct Composites : public CompositeTags::base_t<Composites> {
    using base_t                                = typename CompositeTags::base_t<Composites>;
    constexpr static auto NumPositionEntries    = CompositeTags::NumPositionEntries;
    constexpr static auto NumPositionCovEntries = CompositeTags::NumPositionCovEntries;
    constexpr static auto NumFourMomEntries     = CompositeTags::NumFourMomEntries;
    constexpr static auto NumFourMomCovEntries  = CompositeTags::NumFourMomCovEntries;
    using base_t::allocator_type;

    Composites( unsigned int num_children, Zipping::ZipFamilyNumber zip_identifier = Zipping::generateZipIdentifier(),
                allocator_type alloc = {} )
        : base_t{std::move( zip_identifier ), std::move( alloc )}, m_num_children{num_children} {}
    // Constructor used by zipping machinery when making a copy of a zip
    Composites( Zipping::ZipFamilyNumber zn, Composites const& old )
        : base_t{std::move( zn ), old}, m_num_children{old.m_num_children} {}
    [[nodiscard]] auto numChildren() const { return m_num_children; }

    // Define a custom proxy class for composites, allowing generic code to use
    // uniformly-named accessors.
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct Proxy : LHCb::v2::Event::Proxy<simd, behaviour, ContainerType> {
      using base_t = LHCb::v2::Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::base_t;
      using simd_t = SIMDWrapper::type_map_t<simd>;
      using dType  = simd_t; // for compatibility, TODO remove

    private:
      /** (0
       *   1 2
       *   3 4 5)
       */
      constexpr static std::size_t sym_store_idx( std::size_t x, std::size_t y ) {
        auto col = std::min( x, y );
        auto row = std::max( x, y );
        return row * ( row + 1 ) / 2 + col;
      }

    public:
      // Standard accessors
      [[nodiscard]] auto chi2() const { return this->template get<CompositeTags::Chi2>(); }
      [[nodiscard]] auto nDoF() const { return this->template get<CompositeTags::nDoF>(); }

      [[nodiscard]] auto pid() const { return this->template get<CompositeTags::PID>(); }

      [[nodiscard]] auto e() const { return this->template get<CompositeTags::FourMom>( 3 ); }
      [[nodiscard]] auto px() const { return this->template get<CompositeTags::FourMom>( 0 ); }
      [[nodiscard]] auto py() const { return this->template get<CompositeTags::FourMom>( 1 ); }
      [[nodiscard]] auto pz() const { return this->template get<CompositeTags::FourMom>( 2 ); }
      [[nodiscard]] auto momentum() const { return LHCb::LinAlg::Vec{px(), py(), pz(), e()}; }
      [[nodiscard]] auto threeMomentum() const { return LHCb::LinAlg::Vec{px(), py(), pz()}; }

      [[nodiscard]] auto x() const { return this->template get<CompositeTags::Position>( 0 ); }
      [[nodiscard]] auto y() const { return this->template get<CompositeTags::Position>( 1 ); }
      [[nodiscard]] auto z() const { return this->template get<CompositeTags::Position>( 2 ); }
      [[nodiscard]] auto endVertex() const { return LHCb::LinAlg::Vec{x(), y(), z()}; }

      [[nodiscard]] auto childRelationIndex( std::size_t n_child ) const {
        return this->template get<CompositeTags::ChildRels>( n_child, CompositeTags::ChildRels::IndexOffset );
      }
      [[nodiscard]] auto childRelationFamily( std::size_t n_child ) const {
        return this->template get<CompositeTags::ChildRels>( n_child, CompositeTags::ChildRels::ZipFamilyOffset );
      }
      [[nodiscard]] auto momCovElement( std::size_t i, std::size_t j ) const {
        return this->template get<CompositeTags::FourMomCov>( sym_store_idx( i, j ) );
      }
      [[nodiscard]] auto momPosCovElement( std::size_t i, std::size_t j ) const {
        return this->template get<CompositeTags::PositionFourMomCov>( i, j );
      }

      [[nodiscard]] auto posCovElement( std::size_t i, std::size_t j ) const {
        return this->template get<CompositeTags::PositionCov>( sym_store_idx( i, j ) );
      }
      [[nodiscard]] auto posCovMatrix() const {
        using LHCb::Utils::unwind;
        LHCb::LinAlg::MatSym<decltype( this->posCovElement( 0, 0 ) ), CompositeTags::NumPositionEntries> out;
        unwind<0, CompositeTags::NumPositionEntries>(
            [&]( auto i ) { unwind<0, i + 1>( [&]( auto j ) { out( i, j ) = this->posCovElement( i, j ); } ); } );
        return out;
      }

    private:
      // Helper for returning bits of the momentum covariance matrix
      template <std::size_t FirstFourMomElement, std::size_t NumFourMomEntries>
      [[nodiscard]] auto momCovMatrixImpl() const {
        using LHCb::Utils::unwind;
        LHCb::LinAlg::MatSym<decltype( this->momCovElement( 0, 0 ) ), NumFourMomEntries> out;
        unwind<FirstFourMomElement, FirstFourMomElement + NumFourMomEntries>( [&]( auto i ) {
          unwind<FirstFourMomElement, i + 1>( [&]( auto j ) { out( i, j ) = this->momCovElement( i, j ); } );
        } );
        return out;
      }
      // Helper for returning bits of the momentum - position covariance matrix
      template <std::size_t FirstFourMomElement, std::size_t FirstPosElement, std::size_t NumFourMomEntries,
                std::size_t NumPositionEntries>
      [[nodiscard]] auto momPosCovMatrixImpl() const {
        LHCb::LinAlg::Mat<decltype( this->momPosCovElement( 0, 0 ) ), NumFourMomEntries, NumPositionEntries> out;
        // TODO check the ordering is correct
        using LHCb::Utils::unwind;
        unwind<FirstFourMomElement, FirstFourMomElement + NumFourMomEntries>( [&]( auto i ) {
          unwind<FirstPosElement, FirstPosElement + NumPositionEntries>(
              [&]( auto j ) { out( i, j ) = this->momPosCovElement( i, j ); } );
        } );
        return out;
      }

    public:
      [[nodiscard]] auto momCovMatrix() const { return this->momCovMatrixImpl<0, CompositeTags::NumFourMomEntries>(); }
      // Note tacit assumption that the order is (px, py, pz, pe)
      [[nodiscard]] auto threeMomCovMatrix() const { return this->momCovMatrixImpl<0, 3>(); }
      [[nodiscard]] auto momPosCovMatrix() const {
        // default, {4, 3} shape
        return this->momPosCovMatrixImpl<0, 0, CompositeTags::NumFourMomEntries, CompositeTags::NumPositionEntries>();
      }
      [[nodiscard]] auto threeMomPosCovMatrix() const {
        // ignore the 4th/energy part of the momentum covariance and return a 3x3
        return this
            ->momPosCovMatrixImpl<0, 0, CompositeTags::NumFourMomEntries - 1, CompositeTags::NumPositionEntries>();
      }
      /** Return the full 7x7 covariance matrix.
       *  Order: {x, y, z, px, py, pz, pe}
       */
      [[nodiscard]] auto covMatrix() const {
        LHCb::LinAlg::MatSym<decltype( this->posCovElement( 0, 0 ) ),
                             CompositeTags::NumPositionEntries + CompositeTags::NumFourMomEntries>
            out;
        out = out.template place_at<0, 0>( this->posCovMatrix() );
        out = out.template place_at<CompositeTags::NumPositionEntries, 0>( this->momPosCovMatrix() );
        return out.template place_at<CompositeTags::NumPositionEntries, CompositeTags::NumPositionEntries>(
            this->momCovMatrix() );
      }

      // Accessors for derived quantities
      [[nodiscard]] auto pt() const {
        auto const px = this->px();
        auto const py = this->py();
        return sqrt( px * px + py * py );
      }
      [[nodiscard]] auto mom2() const {
        auto const px = this->px();
        auto const py = this->py();
        auto const pz = this->pz();
        return px * px + py * py + pz * pz;
      }
      [[nodiscard]] auto p() const { return sqrt( this->mom2() ); }
      [[nodiscard]] auto mass2() const {
        auto const energy = this->e();
        return energy * energy - this->mom2();
      }
      [[nodiscard]] auto mass() const { return sqrt( this->mass2() ); }
      // Accessors for container-level information
      [[nodiscard]] auto numChildren() const { return this->container().numChildren(); }
      [[nodiscard]] auto zipIdentifier() const { return this->container().zipIdentifier(); }
    };
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = Proxy<simd, behaviour, ContainerType>;

    template <typename F, typename I, std::size_t NChildren, typename M = std::true_type>
    void emplace_back( LHCb::LinAlg::Vec<F, NumPositionEntries> const& pos,
                       LHCb::LinAlg::Vec<F, NumFourMomEntries> const& p4, I const& pid, F const& chi2, I const& ndof,
                       LHCb::LinAlg::MatSym<F, NumPositionEntries> const&                 pos_cov,
                       LHCb::LinAlg::MatSym<F, NumFourMomEntries> const&                  p4_cov,
                       LHCb::LinAlg::Mat<F, NumFourMomEntries, NumPositionEntries> const& mom_pos_cov,
                       std::array<I, NChildren> const& child_indices, std::array<I, NChildren> const& child_zip_ids,
                       M&& mask = {} ) {
      assert( NChildren == m_num_children );
      auto store = [&]( auto* storage, auto const& val ) {
        if constexpr ( std::is_same_v<std::true_type, std::decay_t<M>> ) {
          // unconditional write
          val.store( storage );
        } else {
          val.compressstore( mask, storage );
        }
      };
      static_assert( !std::is_floating_point_v<F> ); // TODO add support for plain vec<float> etc. input
      auto const old_size     = base_t::size();
      auto const new_elements = [&] {
        if constexpr ( std::is_same_v<std::true_type, std::decay_t<M>> ) {
          return chi2.size();
        } else {
          return popcount( mask );
        }
      }();
      base_t::resize( old_size + new_elements );
      store( std::next( this->data<CompositeTags::PID>(), old_size ), pid );
      store( std::next( this->data<CompositeTags::Chi2>(), old_size ), chi2 );
      store( std::next( this->data<CompositeTags::nDoF>(), old_size ), ndof );
      Utils::unwind<0, NumPositionEntries>(
          [&]( auto i ) { store( std::next( this->data<CompositeTags::Position>( i ), old_size ), pos( i ) ); } );
      Utils::unwind<0, NumPositionCovEntries>( [&]( auto i ) {
        store( std::next( this->data<CompositeTags::PositionCov>( i ), old_size ), pos_cov.m[i] );
      } );
      Utils::unwind<0, NumFourMomEntries>(
          [&]( auto i ) { store( std::next( this->data<CompositeTags::FourMom>( i ), old_size ), p4( i ) ); } );
      Utils::unwind<0, NumFourMomCovEntries>(
          [&]( auto i ) { store( std::next( this->data<CompositeTags::FourMomCov>( i ), old_size ), p4_cov.m[i] ); } );
      // This is using the element ordering imposed by MatVec for a generic
      // matrix, we have to be careful to follow the same convention below.
      // That's not so nice...TODO improve it in future.
      Utils::unwind<0, NumFourMomEntries>( [&]( auto i ) {
        Utils::unwind<0, NumPositionEntries>( [&]( auto j ) {
          store( std::next( this->data<CompositeTags::PositionFourMomCov>( i, j ), old_size ),
                 mom_pos_cov.m[i * NumPositionEntries + j] );
        } );
      } );

      for ( auto i = 0u; i < m_num_children; ++i ) {
        store( std::next( this->data<CompositeTags::ChildRels>( i, CompositeTags::ChildRels::IndexOffset ), old_size ),
               child_indices[i] );
        store(
            std::next( this->data<CompositeTags::ChildRels>( i, CompositeTags::ChildRels::ZipFamilyOffset ), old_size ),
            child_zip_ids[i] );
      }
    }

  private:
    unsigned int m_num_children{};
  };
} // namespace LHCb::v2

REGISTER_PROXY( LHCb::v2::RichPIDs, LHCb::v2::RichPIDProxy );
REGISTER_HEADER( LHCb::v2::RichPIDs, "Event/Particle_v2.h" );

REGISTER_PROXY( LHCb::v2::AssignedMasses, LHCb::v2::AssignedMassProxy );
REGISTER_HEADER( LHCb::v2::AssignedMasses, "Event/Particle_v2.h" );

REGISTER_PROXY( LHCb::v2::ParticleIDs, LHCb::v2::ParticleIDProxy );
REGISTER_HEADER( LHCb::v2::ParticleIDs, "Event/Particle_v2.h" );

REGISTER_PROXY( LHCb::v2::CombDLLs, LHCb::v2::CombDLLProxy );
REGISTER_HEADER( LHCb::v2::CombDLLs, "Event/Particle_v2.h" );

REGISTER_HEADER( LHCb::v2::Composites, "Event/Particle_v2.h" );
template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
struct LHCb::header_map<LHCb::v2::Composites::Proxy<simd, behaviour, ContainerType>> {
  constexpr static auto value = LHCb::header_map_v<LHCb::v2::Composites>;
};

namespace LHCb::v2 {
  // Must instantiate the zip type after the declaration of the proxy types
  // TODO add all the other extrainfo things
  using ChargedBasics =
      Pr::zip_t<Pr::Fitted::Forward::Tracks, RichPIDs, Pr::Muon::PIDs, AssignedMasses, ParticleIDs, CombDLLs>;

  using Particles = LHCb::variant<ChargedBasics, LHCb::v2::Event::Zip<SIMDWrapper::Best, Composites const>>;
} // namespace LHCb::v2
