/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/CaloClusters_v2.h"
#include "GaudiKernel/GaudiException.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/meta_enum.h"

/**
 *
 *
 */

namespace LHCb::Event::Calo {
  inline namespace v2 {

    // Namespace for locations in TDS
    namespace HypothesesLocation {
      inline const std::string Default      = "Rec/Calo/Hypos";
      inline const std::string Photons      = "Rec/Calo/Photons";
      inline const std::string Electrons    = "Rec/Calo/Electrons";
      inline const std::string MergedPi0s   = "Rec/Calo/MergedPi0s";
      inline const std::string SplitPhotons = "Rec/Calo/SplitPhotons";
    } // namespace HypothesesLocation

    //
    namespace Enum {

      /// CaloHypo type enumerations
      meta_enum_class( Hypothesis, unsigned char, Unknown, Undefined, Mip, Photon, PhotonFromMergedPi0,
                       BremmstrahlungPhoton, Pi0Resolved, Pi0Overlapped, Pi0Merged, EmCharged, NeutralHadron,
                       ChargedHadron, Jet )
    } // namespace Enum

    /// Reconstructed CaloHypo class
    class Hypotheses final {
    public:
      using Type = Enum::Hypothesis;

    private:
      struct Hypo_ {
        CaloCellID m_id;
        uint16_t   m_firstClus;
        uint8_t    m_nClus;
        Type       m_hypo;
        Hypo_( CaloCellID id, uint16_t first, uint8_t n, Type h )
            : m_id{id}, m_firstClus{first}, m_nClus{n}, m_hypo{h} {}
        // std::variant< >...
      };

      Clusters           m_clusters;
      std::vector<Hypo_> m_hypos;

      template <typename T>
      class Proxy {
        static_assert( std::is_same_v<std::remove_const_t<T>, Hypotheses> );
        T*           m_parent = nullptr;
        unsigned int m_offset = 0;

        friend std::remove_const_t<T>;

        decltype( auto ) h() const { return m_parent->m_hypos[m_offset]; }

        Proxy( T* parent, unsigned int offset ) : m_parent{parent}, m_offset{offset} {}

      public:
        // allow non-const proxy to be converted to const proxy
        template <typename U,
                  typename = std::enable_if_t<!std::is_same_v<U, T> && std::is_same_v<std::add_const_t<U>, T>>>
        Proxy( Proxy<U> p ) : m_parent{p.m_parent}, m_offset{p.m_offset} {}
        auto operator-> () const { return this; }

        [[nodiscard]] CaloCellID cellID() const { return h().m_id; }
        [[nodiscard]] auto       hypothesis() const { return h().m_hypo; }
        [[nodiscard]] unsigned   size() const { return h().m_nClus; }
        [[nodiscard]] auto       clusters() const {
          auto& c = h();
          return m_parent->m_clusters.range().subspan( c.m_firstClus, c.m_nClus );
        };
        [[nodiscard]] decltype( auto ) position() const {
          auto& c = h();
          switch ( hypothesis() ) {
          case Type::Pi0Merged:
            assert( size() == 3 );
            return m_parent->m_clusters[c.m_firstClus].position();
          default:
            assert( size() == 1 );
            return m_parent->m_clusters[c.m_firstClus].position();
          }
        }
        [[nodiscard]] double energy() const {
          auto& c = h();
          switch ( hypothesis() ) {
          case Type::Pi0Merged:
            assert( size() == 3 );
            return m_parent->m_clusters[c.m_firstClus].e();
          default:
            assert( size() == 1 );
            return m_parent->m_clusters[c.m_firstClus].e();
          }
        }
        [[nodiscard]] double e() const { return energy(); }
      };

      template <typename T>
      class Iterator {
        T*           m_parent = nullptr;
        unsigned int m_offset = 0;
        friend std::remove_const_t<T>;

      public:
        Iterator() = default;

        Iterator( T* parent, unsigned int offset ) : m_parent{parent}, m_offset{offset} {}

        // promote a non-const T iterator to a const T iterator
        template <typename U,
                  typename = std::enable_if_t<!std::is_same_v<U, T> && std::is_same_v<std::add_const_t<U>, T>>>
        Iterator( Iterator<U> iter ) : Iterator{iter.m_parent, iter.m_offset} {}

        auto operator*() const { return Proxy{m_parent, m_offset}; }
        auto operator-> () const { return Proxy{m_parent, m_offset}; }

        Iterator& operator--() {
          --m_offset;
          return *this;
        }

        Iterator& operator++() {
          ++m_offset;
          return *this;
        }

        Iterator& operator+=( int i ) {
          m_offset += i;
          return *this;
        }

        friend int operator-( const Iterator& lhs, const Iterator& rhs ) {
          assert( lhs.m_parent == rhs.m_parent );
          return lhs.m_offset - rhs.m_offset;
        }

        friend bool operator==( const Iterator& lhs, const Iterator& rhs ) {
          assert( lhs.m_parent == rhs.m_parent );
          return lhs.m_offset == rhs.m_offset;
        }

        friend bool operator!=( const Iterator& lhs, const Iterator& rhs ) { return !( lhs == rhs ); }
      };

      template <typename T>
      class Range_ {
        T*           m_parent = nullptr;
        unsigned int m_begin  = 0;
        unsigned int m_end    = 0;

        friend class Hypotheses;
        Range_( T* parent, unsigned int first, unsigned int last ) : m_parent{parent}, m_begin{first}, m_end( last ) {}

      public:
        Range_( Proxy<T> p ) : m_parent{p.m_parent}, m_begin{p.m_offset}, m_end{p.m_offset + 1} {}
        [[nodiscard]] Iterator<T> begin() const { return {m_parent, m_begin}; }
        [[nodiscard]] Iterator<T> end() const { return {m_parent, m_end}; }
        [[nodiscard]] bool        empty() const { return m_begin == m_end; }
        [[nodiscard]] unsigned    size() const {
          assert( m_end >= m_begin );
          return m_end - m_begin;
        }

        [[nodiscard]] Proxy<T> front() const {
          assert( !empty() );
          return {m_parent, m_begin};
        }
        [[nodiscard]] Proxy<T> back() const {
          assert( !empty() );
          return {m_parent, m_end - 1};
        }
        [[nodiscard]] Proxy<T> operator[]( unsigned int idx ) const {
          assert( idx < size() );
          return {m_parent, m_begin + idx};
        }

        [[nodiscard]] Range_ first( unsigned int N ) const {
          assert( N <= size() );
          return {m_parent, m_begin, m_begin + N};
        }
        [[nodiscard]] Range_ last( unsigned int N ) const {
          assert( N <= size() );
          return {m_parent, m_end - N, m_end};
        }
        [[nodiscard]] Range_ subspan( unsigned int first, unsigned int N ) const {
          assert( first + N <= size() );
          return {m_parent, m_begin + first, m_begin + first + N};
        }
      };

    public:
      using reference       = Proxy<Hypotheses>;
      using const_reference = Proxy<const Hypotheses>;
      using Range           = Range_<Hypotheses>;
      using const_Range     = Range_<const Hypotheses>;

      //
      void reserve( std::size_t size ) {
        m_clusters.reserveForClusters( size );
        m_hypos.reserve( size );
      }

      // observers

      [[nodiscard]] auto cbegin() const { return Iterator{this, 0}; }
      [[nodiscard]] auto cend() const { return Iterator{this, static_cast<unsigned int>( m_hypos.size() )}; }
      [[nodiscard]] auto begin() const { return cbegin(); }
      [[nodiscard]] auto end() const { return cend(); }
      [[nodiscard]] auto begin() { return Iterator{this, 0}; }
      [[nodiscard]] auto end() { return Iterator{this, static_cast<unsigned int>( m_hypos.size() )}; }

      [[nodiscard]] bool     empty() const { return m_hypos.empty(); }
      [[nodiscard]] unsigned size() const { return m_hypos.size(); }

      [[nodiscard]] Clusters::const_Range clusters() const { return m_clusters; }
      [[nodiscard]] Clusters::Range       clusters() { return m_clusters; }

      [[nodiscard]] decltype( auto ) clusterContainer() const { return m_clusters; }
      [[nodiscard]] decltype( auto ) clusterContainer() { return m_clusters; }

      // modifiers

      auto push_back( Clusters::const_reference c ) { return m_clusters.emplace_back( c ); }

      auto emplace_back( Type h, LHCb::CaloCellID id, std::initializer_list<Clusters::const_reference> clusters ) {
        assert( clusters.size() != 0 );
        m_hypos.emplace_back( id, static_cast<uint16_t>( m_clusters.size() ), static_cast<uint8_t>( clusters.size() ),
                              h );
        for ( auto c : clusters ) push_back( c );
        return Proxy{this, static_cast<unsigned int>( m_hypos.size() ) - 1};
      }

      Iterator<Hypotheses> erase( Iterator<Hypotheses> it ) {
        // note: we leave the clusters for the erased hypos 'as-is' as otherwise we would have to potentially re-index
        // _all_ clusters... TODO: should we flag the redundant/unused clusters as such?
        assert( it.m_parent == this );
        assert( it != end() );
        m_hypos.erase( std::next( m_hypos.begin(), it.m_offset ) );
        return it; // this iterator now points to the next element...
      }
    };

  } // namespace v2
} // namespace LHCb::Event::Calo

namespace std {
  template <>
  struct iterator_traits<LHCb::Event::Calo::v2::Hypotheses::Iterator<LHCb::Event::Calo::v2::Hypotheses>> {
    using difference_type   = int;
    using iterator_category = random_access_iterator_tag;
  };
  template <>
  struct iterator_traits<LHCb::Event::Calo::v2::Hypotheses::Iterator<const LHCb::Event::Calo::v2::Hypotheses>> {
    using difference_type   = int;
    using iterator_category = random_access_iterator_tag;
  };
} // namespace std
