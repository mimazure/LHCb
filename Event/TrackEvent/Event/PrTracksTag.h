/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/SOACollection.h"
#include "PrTracksInfo.h"

namespace LHCb::v2::Event {
  /**
   * Tag and Proxy baseclasses for representing a 5 parameters state vector (x, y, z, tx, ty)
   */
  template <std::size_t... N>
  struct states_field : floats_field<N..., 5> {
    template <typename Tag, SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour,
              typename ContainerType, typename... Ts>
    struct NDStateProxy {
      using simd_t     = SIMDWrapper::type_map_t<Simd>;
      using type       = typename Tag::template vec_type<simd_t>;
      using OffsetType = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using proxy_type = NDNumericProxy<Tag, Simd, Behaviour, ContainerType, Ts..., std::size_t>;

      NDStateProxy( ContainerType* container, OffsetType offset, Ts... Is )
          : m_container( container ), m_offset( offset ), m_indices( Is... ) {}

      template <auto i>
      [[nodiscard]] constexpr auto proxy() const {
        return std::apply(
            [&]( auto&&... args ) {
              return proxy_type{m_container, m_offset, std::forward<decltype( args )>( args )..., i};
            },
            m_indices );
      }

      [[nodiscard]] constexpr auto x() const { return proxy<0>().get(); }
      [[nodiscard]] constexpr auto y() const { return proxy<1>().get(); }
      [[nodiscard]] constexpr auto z() const { return proxy<2>().get(); }
      [[nodiscard]] constexpr auto tx() const { return proxy<3>().get(); }
      [[nodiscard]] constexpr auto ty() const { return proxy<4>().get(); }

      constexpr void setPosition( type const& x, type const& y, type const& z ) {
        proxy<0>().set( x );
        proxy<1>().set( y );
        proxy<2>().set( z );
      }

      constexpr void setDirection( type const& tx, type const& ty ) {
        proxy<3>().set( tx );
        proxy<4>().set( ty );
      }

      constexpr void set( type const& x, type const& y, type const& z, type const& tx, type const& ty ) {
        setPosition( x, y, z );
        setDirection( tx, ty );
      }

      constexpr auto get() const { return *this; }

    private:
      ContainerType*          m_container;
      OffsetType              m_offset;
      const std::tuple<Ts...> m_indices;
    };

    template <typename Tag, SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour,
              typename ContainerType, typename... Ts>
    using proxy_type = NDStateProxy<Tag, Simd, Behaviour, ContainerType, Ts...>;
  };
  using state_field = states_field<>;
} // namespace LHCb::v2::Event

/**
 * TrackTags for different type with SOACollection
 *
 * @author: Peilian Li (referring to Olli's Particle_v2 CompositeTags structure)
 * @date:   2020-08-07
 */

namespace V2 = LHCb::v2::Event;

// FIXME: define the tags and constants where they are used and remove this namespace:
namespace LHCb::Pr::TracksTag {
  struct trackVP : V2::int_field {};
  struct trackUT : V2::int_field {};
  struct trackSeed : V2::int_field {};
  struct trackFT : V2::int_field {};
  struct StateQoP : V2::float_field {};
  struct nVPHits : V2::int_field {};
  struct nUTHits : V2::int_field {};
  struct nFTHits : V2::int_field {};
  struct Chi2 : V2::float_field {};
  struct Chi2nDoF : V2::int_field {};
  struct Chi2PerDoF : V2::float_field {};

  inline constexpr std::size_t NumPosPars      = 5; // statePosVec vector(5D) (x,y,z,dx,dy)
  inline constexpr std::size_t NumSeedStates   = 3;
  inline constexpr std::size_t NumLongStates   = 2;
  inline constexpr std::size_t NumVeloStates   = 2;
  inline constexpr std::size_t NumCovXY        = 3;
  inline constexpr std::size_t NumVeloStateCov = NumCovXY * NumVeloStates;
  inline constexpr std::size_t MaxVPHits       = static_cast<int>( TracksInfo::MaxHits::VPHits );
  inline constexpr std::size_t MaxUTHits       = static_cast<int>( TracksInfo::MaxHits::UTHits );
  inline constexpr std::size_t MaxFTHits       = static_cast<int>( TracksInfo::MaxHits::FTHits );
} // namespace LHCb::Pr::TracksTag
