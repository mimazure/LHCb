/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Event/SOACollection.h"
#include "Event/Zip.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Vec3.h"
#include "LHCbMath/bit_cast.h"
#include "PrTracksTag.h"
#include "PrVeloTracks.h"
#include "SOAExtensions/ZipUtils.h"

/**
 *
 * Track data for exchanges between UT and FT
 *
 *@author: Arthur Hennequin
 *Based on https://gitlab.cern.ch/lhcb/Rec/blob/master/Pr/PrKernel/PrKernel/PrVeloUTTrack.h
 * from Michel De Cian
 */

namespace V2        = LHCb::v2::Event;
namespace TracksTag = LHCb::Pr::TracksTag;

namespace LHCb::Pr::Upstream {

  namespace Tag {
    using trackVP  = TracksTag::trackVP;
    using StateQoP = TracksTag::StateQoP;
    using nVPHits  = TracksTag::nVPHits;
    using nUTHits  = TracksTag::nUTHits;
    struct vp_indices : V2::ints_field<TracksTag::MaxVPHits> {};
    struct ut_indices : V2::ints_field<TracksTag::MaxUTHits> {};
    struct lhcbIDs : V2::ints_field<TracksTag::MaxVPHits + TracksTag::MaxUTHits> {};
    struct StatePosition : V2::floats_field<TracksTag::NumPosPars> {};
    struct StateCovX : V2::floats_field<TracksTag::NumCovXY> {};

    template <typename T>
    using upstream_t = V2::SOACollection<T, trackVP, StateQoP, nVPHits, nUTHits, vp_indices, ut_indices, lhcbIDs,
                                         StatePosition, StateCovX>;
  } // namespace Tag

  struct Tracks : Tag::upstream_t<Tracks> {
    using base_t = typename Tag::upstream_t<Tracks>;

    constexpr static auto MaxVPHits  = TracksTag::MaxVPHits;
    constexpr static auto MaxUTHits  = TracksTag::MaxUTHits;
    constexpr static auto MaxLHCbIDs = MaxVPHits + MaxUTHits;

    using base_t::allocator_type;

    Tracks( LHCb::Pr::Velo::Tracks const* velo_ancestors,
            Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(), allocator_type alloc = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}, m_velo_ancestors{velo_ancestors} {}

    // Constructor used by zipping machinery when making a copy of a zip
    Tracks( Zipping::ZipFamilyNumber zn, Tracks const& old )
        : base_t{std::move( zn ), old}, m_velo_ancestors{old.m_velo_ancestors} {}

    // Return pointer to ancestor container
    [[nodiscard]] LHCb::Pr::Velo::Tracks const* getVeloAncestors() const { return m_velo_ancestors; };

    template <typename F, typename M = std::true_type>
    void store_StatePosDir( int at, Vec3<F> const& pos, Vec3<F> const& dir, M&& mask = {} ) {
      store<Tag::StatePosition>( at, 0, pos.x, mask );
      store<Tag::StatePosition>( at, 1, pos.y, mask );
      store<Tag::StatePosition>( at, 2, pos.z, mask );
      store<Tag::StatePosition>( at, 3, dir.x, mask );
      store<Tag::StatePosition>( at, 4, dir.y, mask );
    }
    template <typename F, typename M = std::true_type>
    void store_StateCovX( int at, Vec3<F> const& covx, M&& mask = {} ) {
      store<Tag::StateCovX>( at, 0, covx.x, mask );
      store<Tag::StateCovX>( at, 1, covx.y, mask );
      store<Tag::StateCovX>( at, 2, covx.z, mask );
    }
    template <typename I, typename M = std::true_type>
    void store_vp_index( int at, int i, I const& vpidx, M&& mask = {} ) {
      store<Tag::vp_indices>( at, i, vpidx, mask );
    }
    template <typename I, typename M = std::true_type>
    void store_ut_index( int at, int i, I const& utidx, M&& mask = {} ) {
      store<Tag::ut_indices>( at, i, utidx, mask );
    }
    template <typename I, typename M = std::true_type>
    void store_lhcbID( int at, int i, I const& lhcbid, M&& mask = {} ) {
      store<Tag::lhcbIDs>( at, i, lhcbid, mask );
    }

  private:
    LHCb::Pr::Velo::Tracks const* m_velo_ancestors{nullptr};

    DECLARE_PROXY_FRIEND( TrackProxy );
  };
  DECLARE_PROXY( TrackProxy ) {
    PROXY_METHODS( TrackProxy, dType, Tracks, m_Tracks );
    using IType = typename dType::int_v;
    using FType = typename dType::float_v;

  private:
    template <typename Tag, typename... Ts>
    auto loader( Ts... Is ) const { // TODO: move in PROXY_METHODS
      return this->load_vector( this->m_Tracks->template data<Tag>( Is... ) );
    }

  public:
    [[nodiscard]] auto qOverP() const { return loader<Tag::StateQoP>(); }
    [[nodiscard]] auto p() const { return abs( 1.0 / qOverP() ); }
    [[nodiscard]] auto trackVP() const { return loader<Tag::trackVP>(); }
    [[nodiscard]] auto nVPHits() const { return loader<Tag::nVPHits>(); }
    [[nodiscard]] auto nUTHits() const { return loader<Tag::nUTHits>(); }
    [[nodiscard]] auto nHits() const { return nVPHits() + nUTHits(); }

    [[nodiscard]] auto vp_index( std::size_t i ) const { return loader<Tag::vp_indices>( i ); }
    [[nodiscard]] auto ut_index( std::size_t i ) const { return loader<Tag::ut_indices>( i ); }
    [[nodiscard]] auto vp_indices() const {
      std::array<IType, Tracks::MaxVPHits> out = {0};
      for ( auto i = 0; i < nVPHits().hmax( this->loop_mask() ); i++ ) out[i] = vp_index( i );
      return out;
    }
    [[nodiscard]] auto ut_indices() const {
      std::array<IType, Tracks::MaxUTHits> out = {0};
      for ( auto i = 0; i < nUTHits().hmax( this->loop_mask() ); i++ ) out[i] = ut_index( i );
      return out;
    }
    [[nodiscard]] auto lhcbID( std::size_t i ) const { return loader<Tag::lhcbIDs>( i ); }
    [[nodiscard]] auto StatePosElement( std::size_t i ) const { return loader<Tag::StatePosition>( i ); }
    [[nodiscard]] auto StateCovElement( std::size_t i ) const { return loader<Tag::StateCovX>( i ); }
    // Retrieve state info
    [[nodiscard]] auto StatePos() const {
      return Vec3<FType>( StatePosElement( 0 ), StatePosElement( 1 ), StatePosElement( 2 ) );
    }
    [[nodiscard]] auto StateDir() const { return Vec3<FType>( StatePosElement( 3 ), StatePosElement( 4 ), 1.f ); }
    [[nodiscard]] auto StateCov() const {
      return Vec3<FType>( StateCovElement( 0 ), StateCovElement( 1 ), StateCovElement( 2 ) );
    }
    //  Retrieve the (sorted) set of LHCbIDs
    [[nodiscard]] std::vector<LHCbID> lhcbIDs() const {
      std::vector<LHCbID> ids;
      ids.reserve( Tracks::MaxLHCbIDs );
      for ( auto i = 0; i < nHits().cast(); i++ ) {
        static_assert( width() == 1, "lhcbIDs() method cannot be used on vector proxies" );
        ids.emplace_back( LHCb::LHCbID( bit_cast<unsigned int, int>( lhcbID( i ).cast() ) ) );
      }
      return ids;
    }
  };
} // namespace LHCb::Pr::Upstream
REGISTER_PROXY( LHCb::Pr::Upstream::Tracks, LHCb::Pr::Upstream::TrackProxy );
REGISTER_HEADER( LHCb::Pr::Upstream::Tracks, "Event/PrUpstreamTracks.h" );
