/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiSequencer.h"
#include "GaudiKernel/IJobOptionsSvc.h"

//------------------------------------------------------------------------------
//
// Implementation of class :  ProcessPhase
//
// Author:      Marco Cattaneo
// Created:     17th December 1999
//------------------------------------------------------------------------------

/** @class ProcessPhase ProcessPhase.h LHCbAlgs/ProcessPhase.h
 *
 * Processing phase of LHCb application
 *
 * Creates and invokes subdetector processing algorithms
 * Convention: algorithm name = \<phase name\> + \<detector name\>
 *
 * @author: Marco Cattaneo
 * @date:   17th December 1999
 */

class ProcessPhase final : public GaudiSequencer {

public:
  using GaudiSequencer::GaudiSequencer;

  StatusCode initialize() override; ///> Create and initialise sequences of this phase

private:
  Gaudi::Property<std::vector<std::string>> m_detList{this, "DetectorList", {}, "List of subdetectors to be processed"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ProcessPhase )

StatusCode ProcessPhase::initialize() {
  std::string myMeasureProp;

  auto sc = getProperty( "MeasureTime", myMeasureProp );
  if ( !sc ) return sc;

  Gaudi::Property<bool> childMeasureProp( "MeasureTime", false );
  childMeasureProp.fromString( myMeasureProp ).ignore();

  auto jobSvc = service<IJobOptionsSvc>( "JobOptionsSvc" );

  // Declare sequences to the phase
  std::vector<std::string> myMembers;
  myMembers.reserve( m_detList.size() );
  std::transform( begin( m_detList ), end( m_detList ), std::back_inserter( myMembers ),
                  [&jobSvc, &p = childMeasureProp, &baseName = name()]( const std::string& det ) -> std::string {
                    std::string seqName = baseName + det + "Seq";
                    // Sequences are not yet instantiated, so set MeasureTime property directly
                    // in the catalogue. Uses same value as the parent ProcessPhase
                    jobSvc->addPropertyToCatalogue( seqName, p ).ignore();
                    return "GaudiSequencer/" + seqName;
                  } );
  setProperty( "Members", myMembers ).ignore();

  return GaudiSequencer::initialize();
}
