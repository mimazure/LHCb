/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "RichDetectors/RichX.h"

using namespace Rich::Detector;

//=============================================================================
// Ray trace a given direction with the correct PD panel (SIMD)
//=============================================================================
RichX::SIMDRayTResult::Results RichX::rayTrace( const Rich::SIMD::Sides&      sides,       //
                                                const Rich::SIMD::Point<FP>&  pGlobal,     //
                                                const Rich::SIMD::Vector<FP>& vGlobal,     //
                                                Rich::SIMD::Point<FP>&        hitPosition, //
                                                SIMDSmartIDs&                 smartID,     //
                                                SIMDPDs&                      PDs,         //
                                                const LHCb::RichTraceMode     mode ) const {
  using namespace LHCb::SIMD;

  // If all sides are the same, shortcut to a single call
  // hopefully the most common situation ...

  // side 1 mask
  const auto m1 = ( sides == Rich::SIMD::Sides( (int)Rich::firstSide ) );
  if ( all_of( m1 ) ) { return rayTrace( Rich::firstSide, pGlobal, vGlobal, hitPosition, smartID, PDs, mode ); }

  // side 2 mask
  const auto m2 = ( sides == Rich::SIMD::Sides( (int)Rich::secondSide ) );
  if ( all_of( m2 ) ) { return rayTrace( Rich::secondSide, pGlobal, vGlobal, hitPosition, smartID, PDs, mode ); }

  // we have a mixture... So must run both and merge..
  // Is there a better way to handle this ... ?

  // copy input objects for second call
  auto hitPosition2 = hitPosition;
  auto smartID2     = smartID;
  auto PDs2         = PDs;

  // call for the first side
  auto res1 = rayTrace( Rich::firstSide, pGlobal, vGlobal, hitPosition, smartID, PDs, mode );
  // call for the second side
  auto res2 = rayTrace( Rich::secondSide, pGlobal, vGlobal, hitPosition2, smartID2, PDs2, mode );

  // merge results2 into the returned results

  const auto fm2 = LHCb::SIMD::simd_cast<SIMDFP::mask_type>( m2 );
  SIMDFP     hx( hitPosition.x() ), hy( hitPosition.y() ), hz( hitPosition.z() );
  hx( fm2 )   = hitPosition2.x();
  hy( fm2 )   = hitPosition2.y();
  hz( fm2 )   = hitPosition2.z();
  hitPosition = {hx, hy, hz};

  // copy m2 values from res2 to res1
  res1( m2 ) = res2;

  // scalar loop for non-Vc types
  for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
    if ( m2[i] ) {
      // res1[i]    = res2[i];
      smartID[i] = smartID2[i];
      PDs[i]     = PDs2[i];
    }
  }

  // return
  return res1;
}
