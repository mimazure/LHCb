/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <cassert>
#include <cstdint>
#include <map>
#include <ostream>
#include <set>
#include <vector>

// Kernel
#include "Kernel/RichSmartID.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Gaudi
#include "GaudiKernel/CommonMessaging.h"
#include "GaudiKernel/SerializeSTL.h"

// Det Desc
#include "DetDesc/ConditionKey.h"
#include "DetDesc/IConditionDerivationMgr.h"

namespace Rich::Future::DAQ {

  // overloads for vectors etc.
  using GaudiUtils::operator<<;

  /// Helper class for RICH PDMDB readout mapping
  class PDMDBEncodeMapping final {

  public:
    /// Constructor from RICH detector elements
    PDMDBEncodeMapping( const DeRichSystem& richSys ) : m_isInitialised( true ) {
      // load the mapping conditions needed for encoding
      fillRType( richSys );
      fillHType( richSys );
    }

  public:
    // data types

    /// The data for each anode
    class AnodeData final {
    public:
      /// The PDMDB number in a module (0,1)
      Rich::DAQ::PDMDBID pdmdb;
      /// The data frame in a PDMDB (0-5)
      Rich::DAQ::PDMDBFrame frame;
      /// The data frame bit (0-85)
      Rich::DAQ::FrameBitIndex bit;

    public:
      /// Default constructor
      AnodeData() = default;
      /// Constructor from values
      AnodeData( const Rich::DAQ::PDMDBID       p, ///< PDMDB ID
                 const Rich::DAQ::PDMDBFrame    f, ///< PDMDB Frame
                 const Rich::DAQ::FrameBitIndex b  ///< Frame bit
                 )
          : pdmdb( p ), frame( f ), bit( b ) {}

    public:
      /// Check if data is valid
      inline constexpr bool isValid() const noexcept { return ( pdmdb.isValid() && frame.isValid() && bit.isValid() ); }

    public:
      /// ostream operator
      friend std::ostream& operator<<( std::ostream& os, const AnodeData& ad ) {
        return os << "{ PDMDB=" << ad.pdmdb << " Frame=" << ad.frame << " Bit=" << ad.bit << " }";
      }
    };

    /// Array of Anode data for a single PMT
    using PmtData = std::array<AnodeData, LHCb::RichSmartID::MaPMT::TotalPixels>;

    /// Array of PMT data for a single R Type elementary cell
    using RTypeECData = std::array<PmtData, LHCb::RichSmartID::MaPMT::RTypePMTsPerEC>;

    /// Array of EC's per R Type module
    using RTypeModuleData = std::array<RTypeECData, LHCb::RichSmartID::MaPMT::ECsPerModule>;

    /// Array of PMT data for a single H Type elementary cell
    using HTypeECData = std::array<PmtData, LHCb::RichSmartID::MaPMT::HTypePMTsPerEC>;

    /// Array of EC's per H Type module
    using HTypeModuleData = std::array<HTypeECData, LHCb::RichSmartID::MaPMT::ECsPerModule>;

    /// R-Type Module data for each RICH
    using RTypeRichData = DetectorArray<RTypeModuleData>;

  private:
    // defines

    /// Number of Anodes per PMT
    static constexpr auto NumAnodes = LHCb::RichSmartID::MaPMT::TotalPixels;

  private:
    // methods

    /// fill R Type PMT anode map data
    void fillRType( const DeRichSystem& richSys );

    /// fill H Type PMT anode map data
    void fillHType( const DeRichSystem& richSys );

  public:
    // accessors

    /// Access the initialisation state
    inline bool isInitialised() const noexcept { return m_isInitialised; }

    /// Access the Anode data for given channel ID
    const auto& anodeData( const LHCb::RichSmartID id ) const noexcept {
      // Elementary cell in module
      const auto ec = id.elementaryCell();
      // pmt number in EC
      const auto pmt = id.pdNumInEC();
      // Anode index (0-63)
      const auto anode = id.anodeIndex();
      // get PMT type specific data
      if ( UNLIKELY( id.isHTypePMT() ) ) {
        const auto& d = m_hTypeData;
        // sanity checks
        assert( (std::size_t)ec < d.size() );
        assert( (std::size_t)pmt < d[ec].size() );
        assert( (std::size_t)anode < d[ec][pmt].size() );
        assert( d[ec][pmt][anode].isValid() );
        // finally return
        return d[ec][pmt][anode];
      } else {
        const auto& d = m_rTypeData[id.rich()];
        // sanity checks
        assert( (std::size_t)ec < d.size() );
        assert( (std::size_t)pmt < d[ec].size() );
        assert( (std::size_t)anode < d[ec][pmt].size() );
        assert( d[ec][pmt][anode].isValid() );
        // finally return
        return d[ec][pmt][anode];
      }
    }

  public:
    // conditions handling

    /// Creates a condition derivation
    template <typename PARENT>
    static decltype( auto ) addConditionDerivation( PARENT* parent ) {
      // Assume parent algorithm has one and only one input of the correct type...
      return addConditionDerivation( parent, parent->template inputLocation<PDMDBEncodeMapping>() );
    }

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static decltype( auto )                                     //
    addConditionDerivation( PARENT*                     parent, ///< Pointer to parent algorithm
                            LHCb::DetDesc::ConditionKey key     ///< Derived object name
    ) {
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "PDMDBEncodeMapping::addConditionDerivation : Key=" << key << endmsg;
      }
      return LHCb::DetDesc::                                                 //
          addConditionDerivation<PDMDBEncodeMapping( const DeRichSystem& )>( //
              parent->conditionDerivationMgr(),                              //
              std::array{DeRichLocations::RichSystem},                       //
              std::move( key ) );
    }

    /// Default conditions name
    static constexpr const char* DefaultConditionKey = "PDMDBEncodeMapping-Handler";

  private:
    // data

    /// R Type module data
    RTypeRichData m_rTypeData;

    /// H Type module data (only for RICH2)
    HTypeModuleData m_hTypeData;

    /// Flag to indicate initialisation status
    bool m_isInitialised{false};
  };

} // namespace Rich::Future::DAQ
