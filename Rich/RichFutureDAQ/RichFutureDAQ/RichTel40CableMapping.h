/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <cassert>
#include <cstdint>
#include <map>
#include <ostream>
#include <set>
#include <vector>

// Kernel
#include "Kernel/RichSmartID.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Gaudi
#include "GaudiKernel/CommonMessaging.h"
#include "GaudiKernel/SerializeSTL.h"

// Det Desc
#include "DetDesc/ConditionKey.h"
#include "DetDesc/IConditionDerivationMgr.h"

namespace Rich::Future::DAQ {

  // overloads for vectors etc.
  using GaudiUtils::operator<<;

  /// Helper class for RICH PMT data format encoding
  class Tel40CableMapping final {

  public:
    /// Constructor from RICH detector elements
    Tel40CableMapping( const DeRichSystem& richSys ) {
      // load the mapping conditions needed for encoding
      fillCableMaps( richSys );
    }

  public:
    // data types

    /// Struct for storing data for each Tel40 Link
    class Tel40LinkData final {
    public:
      /// RICH SmartID
      LHCb::RichSmartID smartID;
      /// Source ID
      Rich::DAQ::SourceID sourceID;
      /// Tel40 connector
      Rich::DAQ::Tel40Connector connector;
      /// Module Number
      Rich::DAQ::PDModuleNumber moduleNum;
      /// PDMDB number (0,1)
      Rich::DAQ::PDMDBID pdmdbNum;
      /// Link number
      Rich::DAQ::PDMDBFrame linkNum;
      /// PMT type
      bool isHType{false};
      /// Is Link Active
      bool isActive{false};
      /// Module Name
      std::string name{"UNDEFINED"};

    public:
      /// Default constructor
      Tel40LinkData() = default;
      /// Constructor from values
      Tel40LinkData( const std::string&              n,     ///< Tel40 link name
                     const LHCb::RichSmartID         ID,    ///< Cached Smart ID for panel
                     const Rich::DAQ::SourceID       sID,   ///< Source ID
                     const Rich::DAQ::Tel40Connector c,     ///< Tel40 connector
                     const bool                      isH,   ///< Is H type PMT
                     const bool                      act,   ///< Active Link Flag
                     const Rich::DAQ::PDModuleNumber mod,   ///< PDM Module Number
                     const Rich::DAQ::PDMDBID        pdmdb, ///< PDMDB number
                     const Rich::DAQ::PDMDBFrame     link   ///< PDMDB Link (Frame)
                     )
          : smartID( ID )
          , sourceID( sID )
          , connector( c )
          , moduleNum( mod )
          , pdmdbNum( pdmdb )
          , linkNum( link )
          , isHType( isH )
          , isActive( act )
          , name( n ) {}

    public:
      /// Check if data is valid
      inline constexpr bool isValid() const noexcept {
        return ( sourceID.isValid() && connector.isValid() && moduleNum.isValid() && pdmdbNum.isValid() );
      }

    public:
      /// ostream operator
      friend std::ostream& operator<<( std::ostream& os, const Tel40LinkData& td ) {
        return os << "{ " << td.name << " " << td.smartID << " Module=" << td.moduleNum << " PDMDB=" << td.pdmdbNum
                  << " PDMDB-Link=" << td.linkNum << " Active=" << td.isActive << " SourceID=" << td.sourceID
                  << " Tel40-Connector=" << td.connector << " IsH=" << td.isHType << " }";
      }
    };

    /// Max number of links(frames) per PDMDB
    static constexpr const std::size_t MaxLinksPerPDMDB = 6;

    /// Number of PDMDBs per module
    static constexpr const std::size_t PDMDBPerModule = 2;

    /// Number of Tel40 connections per MPO
    static constexpr const std::size_t ConnectionsPerTel40MPO = 12;

    /// Number of Tel40 MPOs
    static constexpr const std::size_t NumberMPOsPerTel40 = 2;

    /// Max number of connections per Tel40
    static constexpr const std::size_t ConnectionsPerTel40 = NumberMPOsPerTel40 * ConnectionsPerTel40MPO;

    /// Maximum Tel40 Source ID
    static constexpr const std::size_t MaxTel40SourceID = 106;

    /// Array of Tel40 for each link in a PDMDB
    using PDMDBLinkData = std::array<Tel40LinkData, MaxLinksPerPDMDB>;

    /// Array of LinkData for each PDMDB in a module
    using PDMDBData = std::array<PDMDBLinkData, PDMDBPerModule>;

    /// Tel40 data for each Module
    using ModuleTel40Data = std::array<PDMDBData, LHCb::RichSmartID::MaPMT::TotalModules>;

    /// Map of active Tel40 Links, per source ID
    using LinksPerSourceID = std::map<Rich::DAQ::SourceID, std::set<Rich::DAQ::Tel40Connector>>;

    /// Array of Tel40 data structs for each connection
    using Tel40ConnectionArray = std::array<Tel40LinkData, ConnectionsPerTel40>;

    /// Array of connection data for each SourceID
    using Tel40SourceIDArray = std::array<Tel40ConnectionArray, MaxTel40SourceID + 1>;

  private:
    // methods

    /// fill Tel40 cable map data
    void fillCableMaps( const DeRichSystem& richSys );

  public:
    // accessors

    /// Access the initialisation state
    inline bool isInitialised() const noexcept { return m_isInitialised; }

    /// Access the Tel40 Link data for given channel ID
    const auto& tel40Data( const LHCb::RichSmartID     id,    // PD ID
                           const Rich::DAQ::PDMDBID    pdmdb, // PDMDB ID
                           const Rich::DAQ::PDMDBFrame frame  // PDMDB Frame
                           ) const noexcept {
      // module number
      const auto modN = id.pdMod();
      // sanity checks
      assert( (std::size_t)modN < m_tel40ModuleData.size() );
      assert( (std::size_t)pdmdb.data() < m_tel40ModuleData[modN].size() );
      assert( (std::size_t)frame.data() < m_tel40ModuleData[modN][pdmdb.data()].size() );
      // return tel40 data
      const auto& data = m_tel40ModuleData[modN][pdmdb.data()][frame.data()];
      assert( data.isValid() );
      // finally return
      return data;
    }

    /// Get the active links per source ID
    const auto& linksPerSourceID() const noexcept { return m_linksPerSourceID; }

    /// Access the Tel40 connection data for a given SourceID
    const auto& tel40Data( const Rich::DAQ::SourceID sID ) const noexcept {
      assert( sID.isValid() );
      assert( (std::size_t)sID.data() < m_tel40ConnData.size() );
      return m_tel40ConnData[sID.data()];
    }

  public:
    // Conditions handling

    /// Creates a condition derivation
    template <typename PARENT>
    static decltype( auto ) addConditionDerivation( PARENT* parent ) {
      // Assume parent algorithm has one and only one input of the correct type...
      return addConditionDerivation( parent, parent->template inputLocation<Tel40CableMapping>() );
    }

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static decltype( auto )                                     //
    addConditionDerivation( PARENT*                     parent, ///< Pointer to parent algorithm
                            LHCb::DetDesc::ConditionKey key     ///< Derived object name
    ) {
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "Tel40CableMapping::addConditionDerivation : Key=" << key << endmsg;
      }
      return LHCb::DetDesc::                                                //
          addConditionDerivation<Tel40CableMapping( const DeRichSystem& )>( //
              parent->conditionDerivationMgr(),                             //
              std::array{DeRichLocations::RichSystem},                      //
              std::move( key ) );
    }

    /// Default conditions name
    static constexpr const char* DefaultConditionKey = "Tel40CableMapping-Handler";

  private:
    // data

    /// Tel40 connection mapping data
    Tel40SourceIDArray m_tel40ConnData;

    /// Tel40 Module Mapping data
    ModuleTel40Data m_tel40ModuleData;

    /// Active links per source ID
    LinksPerSourceID m_linksPerSourceID;

    /// Flag to indicate initialisation status
    bool m_isInitialised{false};
  };

} // namespace Rich::Future::DAQ
