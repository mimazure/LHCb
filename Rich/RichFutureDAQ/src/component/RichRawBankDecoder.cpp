/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <array>
#include <bitset>
#include <cstdint>
#include <limits>
#include <memory>
#include <set>
#include <string>
#include <type_traits>

// Gaudi Array properties ( must be first ...)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Rich (Future) Kernel
#include "RichFutureKernel/RichAlgBase.h"

// DetDesc
#include "DetDesc/ConditionAccessorHolder.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichUtils/RichException.h"
#include "RichUtils/RichHashMap.h"
#include "RichUtils/RichMap.h"

// RICH DAQ
#include "RichFutureDAQ/RichPDMDBDecodeMapping.h"
#include "RichFutureDAQ/RichPackedFrameSizes.h"
#include "RichFutureDAQ/RichTel40CableMapping.h"

// Event model
#include "Event/RawEvent.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Boost
#include "boost/container/small_vector.hpp"
#include "boost/format.hpp"
#include "boost/lexical_cast.hpp"

// Old 32 bit SmartID
#include "Kernel/RichSmartID32.h"

using namespace Rich::Future::DAQ;
using namespace Rich::DAQ;

namespace Rich::Future {

  namespace {

    /// Output data
    using OutData = Rich::Future::DAQ::DecodedData;

    /// Returns the RawBank version enum for the given bank
    inline auto bankVersion( const LHCb::RawBank& bank ) noexcept {
      return static_cast<Rich::DAQ::BankVersion>( bank.version() );
    }

    /// Test if a given bit in a word is set on
    template <std::uint8_t BIT, typename TYPE>
    inline constexpr bool isBitOn( const TYPE data ) noexcept {
      return ( 0 != ( data & ( TYPE( 1 ) << BIT ) ) );
    }

    /// Test if a given bit in a word is set on
    template <typename TYPE, typename POS>
    inline constexpr bool isBitOn( const TYPE data, //
                                   const POS  pos ) noexcept {
      return ( 0 != ( data & ( TYPE( 1 ) << pos ) ) );
    }

    /// Set a bit in a word off
    template <std::uint8_t BIT, typename TYPE>
    inline constexpr void setBitOff( TYPE& data ) noexcept {
      data &= ~( TYPE( 1 ) << BIT );
    }

    /// Type for local decoded SmartID cache
    using DecodedIDs = boost::container::small_vector<LHCb::RichSmartID, 1000>;

  } // namespace

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class RawBankDecoder RichRawBankDecoder.h
   *
   *  RICH Raw bank decoder.
   *
   *  @author Chris Jones
   *  @date   2016-09-21
   */
  class RawBankDecoder final : public Transformer<OutData( const LHCb::RawEvent&,    //
                                                           const DeRichSystem&,      //
                                                           const Tel40CableMapping&, //
                                                           const PDMDBDecodeMapping& ),
                                                  LHCb::DetDesc::usesBaseAndConditions<AlgBase<>,         //
                                                                                       DeRichSystem,      //
                                                                                       Tel40CableMapping, //
                                                                                       PDMDBDecodeMapping>> {

  public:
    // framework

    /// Standard constructor
    RawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data inputs
                       {KeyValue{"RawEventLocation", concat_alternatives( {LHCb::RawEventLocation::Rich,
                                                                           LHCb::RawEventLocation::Default} )},
                        // conditions input
                        KeyValue{"DeRichSystem", DeRichLocations::RichSystem},
                        KeyValue{"Tel40CableMapping", Tel40CableMapping::DefaultConditionKey},
                        KeyValue{"PDMDBDecodeMapping", PDMDBDecodeMapping::DefaultConditionKey}},
                       // output data
                       {KeyValue{"DecodedDataLocation", DecodedDataLocation::Default}} ) {}

    /// Initialize
    StatusCode initialize() override;

    /// Algorithm execution via transform
    OutData operator()( const LHCb::RawEvent&     rawEvent,  //
                        const DeRichSystem&       deRichSys, //
                        const Tel40CableMapping&  tel40Maps, //
                        const PDMDBDecodeMapping& pdmdbMaps ) const override;

  private:
    /// Decoding for MaPMT1 version
    void decodeToSmartIDs_MaPMT1( const LHCb::RawBank&      bank,      //
                                  const DeRichSystem&       deRichSys, //
                                  const Tel40CableMapping&  tel40Maps, //
                                  const PDMDBDecodeMapping& pdmdbMaps, //
                                  DecodedIDs&               decodedIDs ) const;

    /// Decoding for streamed RichSmartIDs version
    void decodeToSmartIDs_StreamIDs( const LHCb::RawBank& bank,      //
                                     const DeRichSystem&  deRichSys, //
                                     DecodedIDs&          decodedIDs ) const;

    /// Fill the output decoded data from a list of Smart IDs
    void fillDecodedData( const DecodedIDs& decodedIDs, //
                          OutData&          decodedData ) const;

    /** Print the given RawBank as a simple hex dump
     *  @param bank The RawBank to dump out
     *  @param os   The Message Stream to print to
     */
    void dumpRawBank( const LHCb::RawBank& bank, MsgStream& os ) const;

  private:
    // properties

    /// Flag to turn on/off decoding of each RICH detector (default is both on)
    Gaudi::Property<Rich::DetectorArray<bool>> m_richIsActive{this, "Detectors", {true, true}};

    /// Flag to activate the raw printout of each Rawbank
    Gaudi::Property<bool> m_dumpBanks{this, "DumpRawBanks", false};

    /** Turn on/off detailed error messages.
     *  VERY verbose in case of frequent errors... */
    Gaudi::Property<bool> m_verboseErrors{this, "VerboseErrors", false};

  private:
    // messaging

    /// error reading flatlist
    mutable WarningCounter m_flatListReadWarn{this, "Invalid RichSmartID read from FlatList data format"};

    /// Bank decoding error
    mutable ErrorCounter m_rawReadErr{this, "Error decoding RawBank"};

    /// Null RawBank pointer
    mutable ErrorCounter m_nullRawBankErr{this, "Retrieved null pointer to RawBank"};

    /// Magic pattern error
    mutable ErrorCounter m_magicErr{this, "Magic Pattern mis-match"};

    /// Not a RICH bank error
    mutable ErrorCounter m_notRichErr{this, "Not a RICH Bank"};

    /// Bank Size MisMatch
    mutable ErrorCounter m_bankSizeMisMatchErr{this, "Decoded Bank Size MisMatch"};

    /// PMT small/large flag mis-match
    mutable ErrorCounter m_pmtSLFlagMismatch{this, "Small/Large PD flag mis-match"};
  };

} // namespace Rich::Future

using namespace Rich::Future;

//=============================================================================

StatusCode RawBankDecoder::initialize() {

  // Initialise base class
  auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // force debug messages
  // sc = setProperty( "OutputLevel", MSG::VERBOSE );

  // derived conditions
  Tel40CableMapping::addConditionDerivation( this );
  PDMDBDecodeMapping::addConditionDerivation( this );

  // report inactive RICHes
  if ( !m_richIsActive[Rich::Rich1] ) { info() << "Decoding for RICH1 disabled" << endmsg; }
  if ( !m_richIsActive[Rich::Rich2] ) { info() << "Decoding for RICH2 disabled" << endmsg; }

  return sc;
}

//=============================================================================

OutData RawBankDecoder::operator()( const LHCb::RawEvent&     rawEvent,  //
                                    const DeRichSystem&       deRichSys, //
                                    const Tel40CableMapping&  tel40Maps, //
                                    const PDMDBDecodeMapping& pdmdbMaps ) const {

  // Get the banks for the Rich
  const auto& richBanks = rawEvent.banks( LHCb::RawBank::Rich );

  // working container for decoded Smart IDs
  // Define here to share allocation across all Source IDs
  DecodedIDs decodedIDs;

  // Make the data map to return
  OutData decodedData;

  // Loop over data banks
  for ( const auto* bank : richBanks ) {

    // test bank is OK
    if ( UNLIKELY( !bank ) ) {
      ++m_nullRawBankErr;
    } else {

      // if configured, dump raw event before decoding
      if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) ) {
        dumpRawBank( *bank, verbose() );
      } else if ( UNLIKELY( m_dumpBanks ) ) {
        dumpRawBank( *bank, info() );
      }

      // basic checks
      bool bankOK = true;
      // Check this is a RICH bank
      if ( UNLIKELY( bank->type() != LHCb::RawBank::Rich ) ) {
        ++m_notRichErr;
        bankOK = false;
      }
      // check bank magic word
      if ( UNLIKELY( LHCb::RawBank::MagicPattern != bank->magic() ) ) {
        ++m_magicErr;
        bankOK = false;
      }

      // If OK, carry on to actual version specific decoding
      if ( LIKELY( bankOK ) ) {

        // clear the ID vector for this bank
        decodedIDs.clear();

        // decoding might throw exceptions so catch and report
        try {
          // Get bank version
          const auto version = bankVersion( *bank );
          // Check version to dispatch to the correct decoding
          if ( LIKELY( MaPMT1 == version ) ) {
            // real PMT bank
            decodeToSmartIDs_MaPMT1( *bank, deRichSys, tel40Maps, pdmdbMaps, decodedIDs );
          } else if ( StreamSmartIDs == version ) {
            // simple 'streamed RichSmartID' version (MC only)
            decodeToSmartIDs_StreamIDs( *bank, deRichSys, decodedIDs );
          } else {
            throw Rich::Exception( "Unknown RICH Tel40 version number " + std::to_string( version ) );
          }
          // Finally, fill the output data from the list of SmartIDs
          fillDecodedData( decodedIDs, decodedData );
        }

        // catch and report any exceptions during decoding
        catch ( const GaudiException& expt ) {
          // count errors
          ++m_rawReadErr;
          _ri_verbo << expt.message() << endmsg;
          // dump the full bank
          if ( m_verboseErrors ) { dumpRawBank( *bank, error() ); }
        }

      } // BANK ok
    }

  } // bank loop

  _ri_debug << "Decoded in total " << richBanks.size() << " RICH Tel40 bank(s)" << endmsg;

  // return the fill map
  return decodedData;
}

//=============================================================================

void RawBankDecoder::decodeToSmartIDs_MaPMT1( const LHCb::RawBank& bank,           //
                                              const DeRichSystem& /* deRichSys */, //
                                              const Tel40CableMapping&  tel40Maps, //
                                              const PDMDBDecodeMapping& pdmdbMaps, //
                                              DecodedIDs&               decodedIDs ) const {

  // sanity check mappings are properly initialised
  assert( tel40Maps.isInitialised() );
  assert( pdmdbMaps.isInitialised() );

  // Get Tel40 Source ID
  const Rich::DAQ::SourceID tel40ID( bank.sourceID() );
  _ri_debug << "MaPMT1 Decoding Tel40 bank " << tel40ID << endmsg;

  // Data bank size in 8 bit words
  const std::size_t bankSize = bank.size();

  // Get the Tel40 DB info for this Source ID
  const auto& connData = tel40Maps.tel40Data( tel40ID );

  // Expected number of data links
  const auto nTel40Links = connData.size();
  // ... number of packed words
  const auto nPackedSizeW = ( nTel40Links / 2 ) + ( nTel40Links % 2 );
  assert( bankSize >= (std::size_t)nPackedSizeW );

  // Array to save sizes for each frame
  std::array<PackedFrameSizes::IntType, Tel40CableMapping::ConnectionsPerTel40> connSizes{};

  // Decode in 8-bit chunks
  using DT             = std::uint8_t;
  const auto NDataBits = std::numeric_limits<DT>::digits;

  // Loop over the first header words to extract the sizes in bytes
  std::size_t iWord{0}, nPayloadWords{0}, iPayloadWord{0};
  const DT*   dataW   = bank.begin<DT>(); // outside for loop as reused below to decode payload
  const DT*   bankEnd = bank.end<DT>();   // cache locally
  for ( ; iWord < nPackedSizeW && dataW != bankEnd; ++dataW, ++iWord, iPayloadWord += 2 ) {
    // Extract the sizes from the packed word
    const PackedFrameSizes sizes( *dataW );
    // extract sizes for each packed value
    const auto s0 = sizes.size0();
    const auto s1 = sizes.size1();
    nPayloadWords += ( s0 + s1 );
    assert( ( std::size_t )( iWord + 1 ) < connSizes.size() );
    connSizes[iPayloadWord]     = s1;
    connSizes[iPayloadWord + 1] = s0;
    //_ri_verbo << "Word " << iWord << " PackedSizeHeader " << sizes << endmsg;
  }

  // Validate the data sizes for each tel40 link extracted from the data
  // is in agreement with the overall bank size
  if ( UNLIKELY( ( nPackedSizeW + nPayloadWords ) != bankSize ) ) {
    ++m_bankSizeMisMatchErr;
    throw Rich::Exception( "BankSize:" + std::to_string( bankSize ) +
                           " != DecodedSize:" + std::to_string( nPackedSizeW + nPayloadWords ) );
  }

  // finally loop over payload words and decode hits
  // note iterator starts from where the above header loop ended...
  std::size_t iLink{0};
  while ( dataW != bankEnd && iLink < connSizes.size() ) {

    //_ri_verbo << "Link " << iLink << " Size " << (int)connSizes[iLink] << endmsg;

    // Do we have any words to decode for this link
    if ( connSizes[iLink] > 0 ) {

      // Get the Tel40 Data for this connection
      const auto& cData = connData[iLink];
      //_ri_verbo << " -> " << cData << endmsg;
      assert( cData.isValid() );

      // get the PDMDB data
      const auto& frameData = pdmdbMaps.getFrameData( cData );

      // Loop over the words for this link
      std::uint16_t iW = 0;
      while ( iW < connSizes[iLink] && dataW != bankEnd ) {

        // check MSB for this word
        const auto isNZS = isBitOn<NDataBits - 1>( *dataW );
        //_ri_verbo << "  -> NZS=" << isNZS << " iW=" << iW << " " //
        //          << boost::format( "%02X" ) % (int)( *dataW )   //
        //          << "(" << std::bitset<NDataBits>( *dataW ) << ")" << endmsg;

        if ( LIKELY( !isNZS ) ) {
          // ZS decoding... word is bit index

          //_ri_verbo << "   -> Bit Index " << (int)*dataW << endmsg;

          // load the anode data for this bit
          assert( ( std::size_t )( *dataW ) < frameData.size() );
          const auto& aData = frameData[*dataW];
          //_ri_verbo << "    -> " << aData << endmsg;
          assert( aData.isValid() );

          // make a smart ID
          auto hitID = cData.smartID; // sets RICH, side, module and PMT type
          // Add the PMT and pixel info
          hitID.setPD_EC_PMT( cData.moduleNum.data(), aData.ec.data(), aData.pmtInEC.data() );
          hitID.setAnode_PMT( aData.anode.data() );
          //_ri_verbo << "     -> " << hitID << endmsg;
          decodedIDs.emplace_back( hitID );

          // move to next word
          ++iW;
          ++dataW;

        } else {
          // NZS decoding...

          // which half of the payload are we in ?
          const bool firstHalf = ( 0 == iW && connSizes[iLink] > 5 );
          // Number of words to decode depends on which half of the payload we are in
          const auto nNZSwords = ( UNLIKELY( firstHalf ) ? 6 : 5 );
          // bit offset per half
          const auto halfBitOffset = ( UNLIKELY( firstHalf ) ? 39 : 0 );

          // look forward last NZS word and read backwards to match frame bit order
          for ( auto iNZS = nNZSwords - 1; iNZS >= 0; --iNZS ) {

            // read the NZS word
            auto nzsW = *( dataW + iNZS );
            // if word zero clear MSB as this is the NZS flag
            if ( UNLIKELY( 0 == iNZS ) ) { setBitOff<NDataBits - 1>( nzsW ); }

            //_ri_verbo << "    -> iNZS=" << iNZS << " "           //
            //          << boost::format( "%02X" ) % (int)( nzsW ) //
            //          << "(" << std::bitset<NDataBits>( nzsW ) << ")" << endmsg;

            // does this word hold any active bits ?
            if ( nzsW > 0 ) {

              // Bit offset for this word
              const auto bitOffset = halfBitOffset + ( NDataBits * ( nNZSwords - 1 - iNZS ) );
              //_ri_verbo << "     -> Bit Offset " << (int)bitOffset << endmsg;

              // word has data so loop over bits to extract
              for ( auto iLB = 0; iLB < NDataBits; ++iLB ) {
                // is bit on ?
                if ( UNLIKELY( isBitOn( nzsW, iLB ) ) ) {

                  // form frame bit value
                  const auto bit = iLB + bitOffset;
                  //_ri_verbo << "      -> Bit Index " << (int)bit << endmsg;

                  // load the anode data for this bit
                  assert( ( std::size_t )( bit ) < frameData.size() );
                  const auto& aData = frameData[bit];
                  //_ri_verbo << "       -> " << aData << endmsg;
                  assert( aData.isValid() );

                  // make a smart ID
                  auto hitID = cData.smartID; // sets RICH, side, module and PMT type
                  // Add the PMT and pixel info
                  hitID.setPD_EC_PMT( cData.moduleNum.data(), aData.ec.data(), aData.pmtInEC.data() );
                  hitID.setAnode_PMT( aData.anode.data() );
                  //_ri_verbo << "        -> " << hitID << endmsg;
                  decodedIDs.emplace_back( hitID );

                } // bit is on
              }   // loop over word bits

            } // word has any data

          } // loop over all NZS words

          // Finally skip the read NZS words
          iW += nNZSwords;
          dataW += nNZSwords;
        }
      }

    } // no data for this link, so just move on

    // move to next link
    ++iLink;

    // info() << cData << endmsg;
  } // data word loop
}

//=============================================================================

void RawBankDecoder::decodeToSmartIDs_StreamIDs( const LHCb::RawBank& bank,      //
                                                 const DeRichSystem&  deRichSys, //
                                                 DecodedIDs&          decodedIDs ) const {

  // Get Tel40 Source ID
  const auto Tel40ID( bank.sourceID() );
  _ri_debug << "FlatList Decoding Tel40 bank " << Tel40ID << endmsg;

  // Data bank size in 32 bit words
  const auto bankSize = bank.size() / 4;

  // reserve size in ID vector for number of hits (bank size here)
  decodedIDs.reserve( bankSize );

  // Loop over bank in 32 bit word chunks
  int lineC( 0 );
  while ( lineC < bankSize ) {

    // Read the smartID direct from the banks
    LHCb::RichSmartID id( LHCb::RichSmartID32( bank.data()[lineC++] ) );

    // work around for some persistent data without 'large' PMT flag set.
    const bool isLarge = deRichSys.isLargePD( id );
    if ( UNLIKELY( id.isLargePMT() != isLarge ) ) {
      ++m_pmtSLFlagMismatch;
      // hack to fix up large PMT flag
      id.setLargePMT( isLarge );
    }

    // Is ID OK ?
    if ( UNLIKELY( !id.isValid() ) ) {
      ++m_flatListReadWarn;
    } else {
      // save to the ID list
      decodedIDs.emplace_back( id );
    }

  } // bank loop
}

//=============================================================================

void RawBankDecoder::fillDecodedData( const DecodedIDs& decodedIDs, //
                                      OutData&          decodedData ) const {

  // cache the last info
  PDInfo*                   last_pdInfo = nullptr;
  ModuleData*               last_mInfo  = nullptr;
  LHCb::RichSmartID         last_pdID;
  Rich::DAQ::PDModuleNumber last_mID;

  // static std::size_t maxNIDs{0};
  // if ( decodedIDs.size() > maxNIDs ) {
  //   maxNIDs = decodedIDs.size();
  //   info() << "#IDs = " << maxNIDs << endmsg;
  // }

  // loop over the IDs to save
  for ( const auto id : decodedIDs ) {

    //_ri_verbo << " -> " << id << endmsg;

    // The module number
    const Rich::DAQ::PDModuleNumber mID( id.pdCol() );

    // The PD ID
    const auto pdID = id.pdID();

    // The RICH and panel
    const auto rich = id.rich();
    const auto side = id.panel();

    // Get the Module Data vector
    auto& mDataV = ( decodedData[rich] )[side];

    // The info objects to fill for this channel
    PDInfo*     pdInfo = nullptr;
    ModuleData* mInfo  = nullptr;

    // Has PD changed ?
    if ( UNLIKELY( pdID != last_pdID || !last_pdInfo || !last_mInfo ) ) {

      // has the module changed ?
      if ( UNLIKELY( mID != last_mID || !last_mInfo ) ) {
        // Find module data
        const auto mIt = std::find_if( mDataV.begin(), mDataV.end(), //
                                       [&mID]( const auto& i ) { return mID == i.moduleNumber(); } );
        // If get here, most likely a new module
        if ( LIKELY( mIt == mDataV.end() ) ) {
          // make a new entry
          mInfo = &mDataV.emplace_back( mID );
          //_ri_verbo << "   -> New Module " << mID << endmsg;
        } else {
          // use found entry
          mInfo = &( *mIt );
          //_ri_verbo << "   -> Found Module " << mID << endmsg;
        }
        // update cached info
        last_mID   = mID;
        last_mInfo = mInfo;
      } else {
        // use cached info
        mInfo = last_mInfo;
        //_ri_verbo << "   -> Cached Module " << mID << endmsg;
      }

      // Find PD data object
      const auto pdIt = std::find_if( mInfo->begin(), mInfo->end(), //
                                      [&pdID]( const auto& i ) { return pdID == i.pdID(); } );
      // If get here most likely new PD
      if ( LIKELY( pdIt == mInfo->end() ) ) {
        // make a new entry
        pdInfo = &mInfo->emplace_back( pdID );
        // Add to active PD count for current rich
        decodedData.addToActivePDs( rich );
        //_ri_verbo << "   -> New PD " << pdID << endmsg;
      } else {
        // Use found entry
        pdInfo = &( *pdIt );
        //_ri_verbo << "   -> Found PD " << pdID << endmsg;
      }
      // update the PD cache
      last_pdID   = pdID;
      last_pdInfo = pdInfo;
    } else {
      // use last PD cache
      pdInfo = last_pdInfo;
      //_ri_verbo << "   -> Cached PD " << pdID << endmsg;
    }

    // add the hit to the list
    pdInfo->smartIDs().emplace_back( id );

    // count the hits
    decodedData.addToTotalHits( rich );

  } // loop over IDs
}

//=============================================================================

void RawBankDecoder::dumpRawBank( const LHCb::RawBank& bank, MsgStream& os ) const {

  // Get bank version and ID
  const auto sID( bank.sourceID() );
  const auto version = bankVersion( bank );

  // Data bank size in 8-bit words
  const auto bankSize = bank.size();

  const std::string& LINES = "-------------------------------------------------------------------"
                             "-------------------------------------------";
  os << LINES << endmsg;
  os << "RawBank version=" << version << " SourceID=" << sID << " datasize(bytes)=" //
     << bankSize << " magic=" << boost::format( "%04X" ) % bank.magic() << endmsg;
  os << LINES << endmsg;

  // Is this an empty bank ?
  if ( bankSize > 0 ) {
    using Dtype                     = std::uint8_t;
    const unsigned int cli_rowwidth = 8;
    std::size_t        iWord        = 0;
    for ( const Dtype* dataW = bank.begin<Dtype>(); dataW != bank.end<Dtype>(); ++dataW ) {
      // start new line
      if ( iWord % cli_rowwidth == 0 ) {
        if ( 0 != iWord ) { os << endmsg; }
        os << boost::format( "%04X | " ) % iWord;
      }
      // print the byte
      os << boost::format( "%02X" ) % (int)( *dataW );
      os << "(" << std::bitset<8>( *dataW ) << ") ";
      // increment count
      ++iWord;
    }
    os << endmsg;

  } else {
    os << "  -> Bank is empty" << endmsg;
  }

  os << LINES << endmsg;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RawBankDecoder )

//=============================================================================
