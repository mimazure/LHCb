/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Kernel
#include "Kernel/RichSmartID.h"

// Event model
#include "Event/MCRichOpticalPhoton.h"

// Rich Utils
#include "RichUtils/RichMap.h"

namespace Rich::Future::MC::Relations {

  /// Helper class for RichMCHits
  class MCOpticalPhotonUtils {

  public:
    /// Constructor from MC Optical Photons
    MCOpticalPhotonUtils( const LHCb::MCRichOpticalPhotons& mcphotons );

  public:
    // access methods

    /// Finds the MCRichOpticalPhoton associated to a given MCRichHit
    decltype( auto ) mcOpticalPhoton( const LHCb::MCRichHit* mcHit ) const {
      const auto i = m_hitToPhot.find( mcHit );
      return ( i != m_hitToPhot.end() ? i->second : nullptr );
    }

    /// Finds the MCRichOpticalPhotons associated to a list of MCRichHits
    template <typename HITS>
    decltype( auto ) mcOpticalPhotons( const HITS& hits ) const {
      LHCb::MCRichOpticalPhoton::ConstVector photons;
      photons.reserve( hits.size() );
      for ( const auto hit : hits ) {
        const auto phot = mcOpticalPhoton( hit );
        if ( phot ) { photons.push_back( phot ); }
      }
      return photons;
    }

  private:
    // types

    /// Type for mapping from MCRichHit to MCRichOpticalPhoton
    using MCRichHitToOpPhot = Rich::Map<const LHCb::MCRichHit*, const LHCb::MCRichOpticalPhoton*>;

  private:
    // cached data

    /// Mapping of hits to photons
    MCRichHitToOpPhot m_hitToPhot;
  };

} // namespace Rich::Future::MC::Relations
