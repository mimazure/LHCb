###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: ZeroMQ
################################################################################

gaudi_subdir(ZeroMQ v4r1p5)

gaudi_depends_on_subdirs(GaudiKernel
                         GaudiAlg)

find_package(Boost COMPONENTS serialization iostreams)
find_package(ROOT COMPONENTS Core RIO Hist)
find_package(ZMQ)
find_package(AIDA)
find_library(SODIUM_LIBRARY NAMES sodium
  HINTS $ENV{libsodium_ROOT_DIR}/lib ${libsodium_ROOT_DIR}/lib)

gaudi_install_headers(ZeroMQ)

# Library for OMQ
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS} ${ZMQ_INCLUDE_DIRS})

# Library for our extra stuff
gaudi_add_library(ZMQLib lib/*.cpp
                  PUBLIC_HEADERS zmq
                  LINK_LIBRARIES ${ZMQ_LIBRARIES})

if(SODIUM_LIBRARY)
  message(STATUS "Found sodium: ${SODIUM_LIBRARY}")
  get_filename_component(SODIUM_LIBRARY_DIR "${SODIUM_LIBRARY}" PATH)
  gaudi_env(APPEND LD_LIBRARY_PATH "${SODIUM_LIBRARY_DIR}")
  target_link_libraries(ZMQLib ${SODIUM_LIBRARY})
endif()

# Implementation of ZeroMQSvc
gaudi_add_module(ZeroMQ component/*.cpp
                 LINK_LIBRARIES Boost AIDA ROOT GaudiKernel ZMQLib ${ZMQ_LIBRARIES} GaudiAlgLib)

gaudi_install_python_modules()

gaudi_add_test(QMTest QMTEST)
