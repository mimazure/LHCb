/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <array>
#include <map>
#include <string>
#include <tuple>
#include <unordered_set>
#include <vector>

#include <boost/functional/hash.hpp>

#include <GaudiAlg/GaudiAlgorithm.h>

#include <ZeroMQ/IZeroMQSvc.h>
#include <ZeroMQ/SerializeSize.h>

namespace {
  using std::array;
  using std::make_tuple;
  using std::map;
  using std::pair;
  using std::string;
  using std::tuple;
  using std::unordered_set;
  using std::vector;
} // namespace

/** @class TestSerializationAlgo TestSerializationAlgo.h
 *
 *
 *  @author Roel Aaij
 *  @date   2016-06-01
 */
class TestSerializationAlgo : public GaudiAlgorithm {
public:
  /// Standard constructor
  TestSerializationAlgo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

  SmartIF<IZeroMQSvc>& zmq() const {
    if ( !m_zmqSvc ) { m_zmqSvc = service( "ZeroMQSvc" ).as<IZeroMQSvc>(); }
    return m_zmqSvc;
  }

private:
  mutable SmartIF<IZeroMQSvc> m_zmqSvc;

  template <class T, class Comp = std::equal_to<T>>
  void testEncodeDecode( const T& test, Comp comp = Comp{} ) {
    auto msg = zmq()->encode( test );
    auto tmp = zmq()->decode<T>( msg );
    auto tn  = System::typeinfoName( typeid( T ) );
    if ( comp( test, tmp ) ) {
      info() << "Message size: " << msg.size() << " for " << tn << endmsg;
    } else {
      error() << "Encode decode test failed for " << tn << endmsg;
    }
  }
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TestSerializationAlgo )

//=============================================================================
TestSerializationAlgo::TestSerializationAlgo( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {}

//=============================================================================
StatusCode TestSerializationAlgo::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  testEncodeDecode( 25ul );
  testEncodeDecode( 25. );
  testEncodeDecode( string{"test_string"} );
  testEncodeDecode( vector<int>{1, 2, 3, 6} );
  testEncodeDecode( array<int, 4>{1, 2, 3, 6} );
  testEncodeDecode( vector<tuple<int, double, string>>{make_tuple( 1, 2., "one" ), make_tuple( 2, 3., "two" ),
                                                       make_tuple( 3, 8., "three" ), make_tuple( 5, 6., "four" )} );
  testEncodeDecode( map<int, string>{{1, "one"}, {2, "two"}} );
  testEncodeDecode( unordered_set<string>{"one", "five", "twelve", "twenty"} );
  testEncodeDecode( unordered_set<pair<unsigned long, string>, boost::hash<pair<unsigned long, string>>>{
      {1, "one"}, {5, "five"}, {12, "twelve"}, {20, "twenty"}} );

  return StatusCode::SUCCESS;
}
