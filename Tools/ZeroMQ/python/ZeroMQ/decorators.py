###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import cppyy
import ctypes
import pickle
import errno
from functools import wraps
import threading

gbl = cppyy.gbl

gbl.ROOT.EnableThreadSafety()
gInterpreter = gbl.gInterpreter
gInterpreter.Load('libzmq.so')
gInterpreter.Load('libZMQ.so')
gInterpreter.Declare('#include <ZeroMQ/IZeroMQSvc.h>')
gInterpreter.Declare('#include <ZeroMQ/ZeroMQHelper.h>')
gInterpreter.Declare('#include <ZeroMQ/ZeroMQPoller.h>')
gInterpreter.Declare('#include <ZeroMQ/functions.h>')

# Decorate ZeroMQSvc
IZeroMQSvc = cppyy.gbl.IZeroMQSvc
Helper = cppyy.gbl.ZeroMQHelper
TObject = cppyy.gbl.TObject
zmq = cppyy.gbl.zmq

_ctype_table = dict([('b', 'char'), ('B', 'unsigned char'), ('h', 'short'),
                     ('H', 'unsigned short'), ('i', 'int'),
                     ('I', 'unsigned int'), ('l', 'long'),
                     ('L', 'unsigned long'), ('l', 'long long'),
                     ('L', 'unsigned long long'), ('f', 'float'),
                     ('d', 'double'), ('g', 'long double')])


def static_vars(**kwargs):
    """ Add an attribute to the function that can be used as a static
    variable. """

    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func

    return decorate


def protect_interrupt(func):
    """ Protect agains interrupted systemcall exception by trying again."""

    @wraps(func)
    def wrapper(*args, **kwargs):
        while True:
            try:
                return func(*args, **kwargs)
            except IOError, e:
                if e.errno != errno.EINTR:
                    raise

    return wrapper


@static_vars(svc=None)
def _get_svc():
    if _get_svc.svc is None:
        from GaudiPython.Bindings import AppMgr, InterfaceCast
        gaudi = AppMgr()
        if gaudi.FSMState() < cppyy.gbl.Gaudi.StateMachine.INITIALIZED:
            gaudi.initialize()
        svc = gaudi.service("ZeroMQSvc", cppyy.gbl.IZeroMQSvc)
        if svc is None:
            svc = gaudi.createSvc("ZeroMQSvc")
            svc = InterfaceCast(cppyy.gbl.IZeroMQSvc)(svc)
        _get_svc.svc = svc
        assert (_get_svc.svc is not None)
    return _get_svc.svc


def _recv(self, T):
    recv = None
    if (type(T) == str and hasattr(cppyy.gbl, T)
            and issubclass(getattr(cppyy.gbl, T), TObject)):
        recv = Helper(T).receiveROOT
    elif type(T) is not str and issubclass(T, ctypes._SimpleCData):
        recv = Helper(_ctype_table[T._type_]).receive
    elif hasattr(T, '_type_') and type(T._type_) == str:
        recv = Helper(T._type_).receive
    else:
        recv = Helper(T).receive
    # This method may block, so inform TMethodProxy that the GIL should be
    # released
    recv._threaded = True
    recv = protect_interrupt(recv)
    r = recv(_get_svc(), self)
    self._more = r.second
    return r.first


def _decode(self, T):
    svc = _get_svc()
    if type(T) == str and hasattr(cppyy.gbl, T) \
       and issubclass(getattr(cppyy.gbl, T), TObject):
        return lambda *args: Helper(T).decodeROOT(svc, *args)
    else:
        return lambda *args: Helper(T).decode(svc, *args)


def _send(self, T, flags=0):
    svc = _get_svc()
    snd = None
    if isinstance(T, TObject):
        snd = Helper(TObject).send
    elif type(T) is not str and issubclass(type(T), ctypes._SimpleCData):
        snd = Helper(_ctype_table[T._type_]).send
        T = T.value
    elif hasattr(type(T), '_type_') and type(type(T)._type_) == str:
        recv = Helper(T._type_).send
    else:
        snd = Helper(type(T)).send
    snd._threaded = True
    return protect_interrupt(snd)(svc, self, T, flags)


def _encode(self, T):
    svc = _get_svc()
    if isinstance(T, TObject):
        return Helper(TObject).encode(svc, T)
    else:
        return Helper(type(T)).encode(svc, T)


def _send_multipart(self, msgs, flags=0):
    r = False
    for i in range(len(msgs) - 1):
        r |= _send(self, msgs[i], flags | zmq.SNDMORE)
    r |= _send(self, msgs[-1], flags & ~zmq.SNDMORE)
    return r


def _send_pyobj(self, o, *args):
    msg = pickle.dumps(o)
    return self.send(msg, *args)


def _recv_pyobj(self):
    msg = self.recv_string()
    return pickle.loads(msg)


def _recv_string(self):
    return _recv(self, cppyy.gbl.std.string)


def _recv_message(self):
    return _recv(self, zmq.message_t)


def _socket_init(self, context, t):
    self._more = False
    self._cpp_init(context, t)


def _close(self, linger=None):
    if linger is not None:
        self.setsockopt(zmq.LINGER, linger)
    self._cpp_close()


# Poller methods
def _init_(self):
    self._sockets = {}
    self._cpp_init_()


def _register(self, socket, flags):
    i = None
    if isinstance(socket, zmq.socket_t):
        i = self.register_socket(socket, flags)
    elif hasattr(socket, 'fileno'):
        i = self.register_socket(socket.fileno(), flags)
    elif type(socket) == int:
        try:
            os.fstat(socket)
            i = self.register_socket(socket, flags)
        except OSError:
            pass
    if i is None:
        raise TypeError("Socket must be either zmq::socket_t, have a"
                        " fileno function that returns a valid file"
                        "descriptor, or be a valid file descriptor.")
    self._sockets[int(i)] = socket


def _unregister(self, socket):
    i = None
    if isinstance(socket, zmq.socket_t):
        i = self.unregister_socket(socket)
    elif hasattr(socket, 'fileno'):
        i = self.unregister_socket(socket.fileno())
    elif type(socket) == int:
        i = self.unregister_socket(socket)
    else:
        raise TypeError("Socket must be either zmq::socket_t, have a"
                        " fileno function that returns a valid file"
                        "descriptor, or be a valid file descriptor.")
    self._sockets.pop(int(i))


@protect_interrupt
def _poll(self, timeout=-1):
    sockets = self._cpp_poll_(timeout)
    return {self._sockets[int(s.first)]: s.second for s in sockets}


Poller = cppyy.gbl.ZeroMQPoller
Poller._cpp_init_ = Poller.__init__
Poller.__init__ = _init_
Poller._cpp_poll_ = Poller.poll
Poller._cpp_poll_._threaded = True
Poller.poll = _poll
Poller.register = _register
Poller.unregister = _unregister

zmq.Poller = Poller

# Decorate socket_t
socket_t = zmq.socket_t
socket_t._cpp_init = socket_t.__init__
socket_t.__init__ = _socket_init
socket_t._cpp_close = socket_t.close
socket_t.close = _close
socket_t.setsockopt = zmq.setsockopt
socket_t.recv = _recv
socket_t.recv_string = _recv_string
socket_t.recv_pyobj = _recv_pyobj
socket_t.recv_message = _recv_message
socket_t.decode = _decode
socket_t.encode = _encode
socket_t.send = _send
socket_t.send_string = _send
socket_t.send_pyobj = _send_pyobj
socket_t.send_multipart = _send_multipart
socket_t.more = lambda self: self._more


@static_vars(gaudi=None)
def _context_init(self):
    # Get the ZeroMQSvc
    svc = _get_svc()
    # We need Gaudi to start it if needed
    if _context_init.gaudi is None:
        from GaudiPython.Bindings import AppMgr
        _context_init.gaudi = AppMgr()
    # We need to start Gaudi it if hasn't been started
    if _context_init.gaudi.FSMState() < cppyy.gbl.Gaudi.StateMachine.RUNNING:
        _context_init.gaudi.start()
    self._cpp_init()


# decorate context
context_t = zmq.context_t
context_t._cpp_init = context_t.__init__
context_t.__init__ = _context_init
context_t.socket = lambda self, t: socket_t(self, t)

zmq.Context = context_t

__all__ = ('zmq', )
