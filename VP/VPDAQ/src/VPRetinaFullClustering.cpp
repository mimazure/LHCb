/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/VPFullCluster.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "Kernel/VPChannelID.h"
#include "Kernel/VPConstants.h"
#include "VPDet/DeVP.h"
#include "VPKernel/PixelUtils.h"
#include "VPKernel/VeloPixelInfo.h"
#include "VPRetinaFullCluster.h"
#include "VPRetinaMatrix.h"
#include <array>
#include <iomanip>
#include <iterator>
#include <tuple>
#include <vector>

/** @class VPRetinaFullClustering VPRetinaFullClustering.h
 * @author Federico Lazzari
 * @date   2018-06-20
 */

// Namespace for locations in TDS
namespace LHCb {
  namespace VPFullClusterLocation {
    inline const std::string Offsets = "Raw/VP/FullClustersOffsets";
  }
} // namespace LHCb

namespace LHCb::VP::Retina {
  struct GeomCache {
    GeomCache( const DeVP& det );
    std::array<std::array<float, 9>, ::VP::NSensors> m_ltg;
    LHCb::span<const double>                         m_local_x;
    LHCb::span<const double>                         m_x_pitch;
    float                                            m_pixel_size;
  };
} // namespace LHCb::VP::Retina

class VPRetinaFullClustering
    : public Gaudi::Functional::MultiTransformer<
          std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>(
              const LHCb::RawEvent&, const LHCb::VP::Retina::GeomCache& ),
          LHCb::DetDesc::usesConditions<LHCb::VP::Retina::GeomCache>> {

public:
  /// Standard constructor
  VPRetinaFullClustering( const std::string& name, ISvcLocator* pSvcLocator );

  /// Algorithm initialization
  StatusCode initialize() override;

  /// Algorithm execution
  std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>
  operator()( const LHCb::RawEvent&, const LHCb::VP::Retina::GeomCache& ) const override;

private:
  /// bank version. (change this every time semantics change!)
  static constexpr unsigned int m_bankVersion = 1;

  /// make RetinaClusters from bank
  std::vector<LHCb::VPRetinaFullCluster> makeRetinaFullClusters( LHCb::span<const uint32_t> bank,
                                                                 const unsigned int         sensor ) const;

  Gaudi::Property<unsigned int> m_chain_length{
      this, "chain_length", 20,
      "Number of RetinaMatrix in a chain, a lower number increase clone, a bigger number require more space in FPGA"};
};

using namespace LHCb;

DECLARE_COMPONENT( VPRetinaFullClustering )

namespace {
  struct SPCache {
    std::array<float, 4> fxy;
    unsigned char        pattern;
    unsigned char        nx1;
    unsigned char        nx2;
    unsigned char        ny1;
    unsigned char        ny2;
  };
  //=========================================================================
  // Cache Super Pixel cluster patterns.
  //=========================================================================
  auto create_SPPatterns() {
    std::array<SPCache, 256> SPCaches;
    // create a cache for all super pixel cluster patterns.
    // this is an unoptimized 8-way flood fill on the 8 pixels
    // in the super pixel.
    // no point in optimizing as this is called once in
    // initialize() and only takes about 20 us.

    // define deltas to 8-connectivity neighbours
    const int dx[] = {-1, 0, 1, -1, 0, 1, -1, 1};
    const int dy[] = {-1, -1, -1, 1, 1, 1, 0, 0};

    // clustering buffer for isolated superpixels.
    unsigned char sp_buffer[8];

    // SP index buffer and its size for single SP clustering
    unsigned char sp_idx[8];
    unsigned char sp_idx_size = 0;

    // stack and stack pointer for single SP clustering
    unsigned char sp_stack[8];
    unsigned char sp_stack_ptr = 0;

    // loop over all possible SP patterns
    for ( unsigned int sp = 0; sp < 256; ++sp ) {
      sp_idx_size = 0;
      for ( unsigned int shift = 0; shift < 8; ++shift ) {
        const unsigned char p = sp & ( 1 << shift );
        sp_buffer[shift]      = p;
        if ( p ) { sp_idx[sp_idx_size++] = shift; }
      }

      // loop over pixels in this SP and use them as
      // cluster seeds.
      // note that there are at most two clusters
      // in a single super pixel!
      unsigned char clu_idx = 0;
      for ( unsigned int ip = 0; ip < sp_idx_size; ++ip ) {
        unsigned char idx = sp_idx[ip];

        if ( 0 == sp_buffer[idx] ) { continue; } // pixel is used

        sp_stack_ptr             = 0;
        sp_stack[sp_stack_ptr++] = idx;
        sp_buffer[idx]           = 0;
        unsigned char x          = 0;
        unsigned char y          = 0;
        unsigned char n          = 0;

        while ( sp_stack_ptr ) {
          idx                     = sp_stack[--sp_stack_ptr];
          const unsigned char row = idx % 4;
          const unsigned char col = idx / 4;
          x += col;
          y += row;
          ++n;

          for ( unsigned int ni = 0; ni < 8; ++ni ) {
            const char ncol = col + dx[ni];
            if ( ncol < 0 || ncol > 1 ) continue;
            const char nrow = row + dy[ni];
            if ( nrow < 0 || nrow > 3 ) continue;
            const unsigned char nidx = ( ncol << 2 ) | nrow;
            if ( 0 == sp_buffer[nidx] ) continue;
            sp_stack[sp_stack_ptr++] = nidx;
            sp_buffer[nidx]          = 0;
          }
        }

        const uint32_t cx = x / n;
        const uint32_t cy = y / n;
        const float    fx = x / static_cast<float>( n ) - cx;
        const float    fy = y / static_cast<float>( n ) - cy;

        // store the centroid pixel
        SPCaches[sp].pattern |= ( ( cx << 2 ) | cy ) << 4 * clu_idx;

        // set the two cluster flag if this is the second cluster
        SPCaches[sp].pattern |= clu_idx << 3;

        // set the pixel fractions
        SPCaches[sp].fxy[2 * clu_idx]     = fx;
        SPCaches[sp].fxy[2 * clu_idx + 1] = fy;

        // increment cluster count. note that this can only become 0 or 1!
        ++clu_idx;
      }
    }
    return SPCaches;
  }
  // SP pattern buffers for clustering, cached once.
  // There are 256 patterns and there can be at most two
  // distinct clusters in an SP.
  static const std::array<SPCache, 256> s_SPCaches = create_SPPatterns();
} // namespace

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPRetinaFullClustering::VPRetinaFullClustering( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer( name, pSvcLocator,
                        {KeyValue{"RawEventLocation", LHCb::RawEventLocation::Default},
                         KeyValue{"GeomCacheLocation", "VPRetinaFullClustering-" + name + "-GeomCache"}},
                        {KeyValue{"ClusterLocation", LHCb::VPFullClusterLocation::Default},
                         KeyValue{"ClusterOffsets", LHCb::VPFullClusterLocation::Offsets}} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode VPRetinaFullClustering::initialize() {
  return MultiTransformer::initialize().andThen( [&] {
    addConditionDerivation<LHCb::VP::Retina::GeomCache( const DeVP& )>( {DeVPLocation::Default},
                                                                        inputLocation<LHCb::VP::Retina::GeomCache>() );
  } );
}

//============================================================================
// Rebuild the geometry (in case something changes in the Velo during the run)
//============================================================================
LHCb::VP::Retina::GeomCache::GeomCache( const DeVP& det ) {

  m_local_x    = det.sensor( 0 ).xLocal();
  m_x_pitch    = det.sensor( 0 ).xPitch();
  m_pixel_size = det.sensor( 0 ).pixelSize();

  det.runOnAllSensors( [this]( const DeVPSensor& sensor ) {
    // get the local to global transformation matrix and
    // store it in a flat float array of sixe 12.
    Gaudi::Rotation3D     ltg_rot;
    Gaudi::TranslationXYZ ltg_trans;
    sensor.getGlobalMatrixDecomposition( ltg_rot, ltg_trans );
    auto& ltg = m_ltg[sensor.sensorNumber()];
    ltg_rot.GetComponents( ltg.data() );
    ltg[2] = ltg_trans.X();
    ltg[5] = ltg_trans.Y();
    ltg[8] = ltg_trans.Z();
  } );
}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>> VPRetinaFullClustering::
                                                                                                operator()( const LHCb::RawEvent& rawEvent, const LHCb::VP::Retina::GeomCache& cache ) const {
  auto result = std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>{};

  const auto& tBanks = rawEvent.banks( LHCb::RawBank::VP );
  if ( tBanks.empty() ) return result;

  const unsigned int version = tBanks[0]->version();
  if ( version != 2 ) {
    warning() << "Unsupported raw bank version (" << version << ")" << endmsg;
    return result;
  }

  // Since the pool is local, to first preallocate the pool, then count hits per module,
  // and then preallocate per module and move hits might not be faster than adding
  // directly to the PixelModuleHits (which would require more allocations, but
  // not many if we start with a sensible default)
  auto& [pool, offsets] = result;
  const unsigned int startSize = 10000U;
  pool.reserve( startSize );

  // 1 module = N Cluster -> N x M channelIDs
  std::vector<std::vector<LHCb::VPChannelID>> channelIDs;
  channelIDs.reserve( 400 );

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Read " << tBanks.size() << " raw banks from TES" << endmsg; }

  // Loop over VP RawBanks
  for ( auto iterBank : tBanks ) {

    const unsigned int sensor = iterBank->sourceID();
    const unsigned int module = 1 + ( sensor / ::VP::NSensorsPerModule );
    int                n_hits = 0;

    const auto& ltg = cache.m_ltg[sensor];
    auto        retinaClusters = makeRetinaFullClusters( iterBank->range<uint32_t>(), sensor );

    for ( auto iterCluster : retinaClusters ) {
      const uint32_t cx = iterCluster.word() >> 14 & 0x3FF;
      const float    fx = ( ( iterCluster.word() >> 11 ) & 0x7 ) / 8.;
      const uint32_t cy = ( iterCluster.word() >> 3 ) & 0xFF;
      const float    fy = ( iterCluster.word() & 0x7 ) / 8.;

      const uint32_t chip = cx / Pixel::CHIP_COLUMNS;
      const uint32_t ccol = cx % Pixel::CHIP_COLUMNS;

      LHCb::VPChannelID cid( sensor, chip, ccol, cy );
      channelIDs.push_back( iterCluster.channelIDs() );

      const float local_x = cache.m_local_x[cx] + fx * cache.m_x_pitch[cx];
      const float local_y = ( cy + 0.5 + fy ) * cache.m_pixel_size;

      const float gx = ltg[0] * local_x + ltg[1] * local_y + ltg[2];
      const float gy = ltg[3] * local_x + ltg[4] * local_y + ltg[5];
      const float gz = ltg[6] * local_x + ltg[7] * local_y + ltg[8];

      pool.emplace_back( fx, fy, gx, gy, gz, cid, std::move( channelIDs.back() ) );
      n_hits++;
      ++offsets[module];
    }

  } // loop over all banks

  std::partial_sum( offsets.begin(), offsets.end(), offsets.begin() );
  // Do we actually need to sort the hits ? [ depends, if the offline clustering will be re-run and tracking will use
  // those clusters , yes, otherwise no ]
  for ( size_t moduleID = 0; moduleID < VeloInfo::Numbers::NModules; ++moduleID ) {
    // In even modules you fall in the branching at -180, 180 degrees, you want to do that continuos
    std::stable_sort(
        pool.begin() + offsets[moduleID], pool.begin() + offsets[moduleID + 1],
        []( const LHCb::VPFullCluster& a, const LHCb::VPFullCluster& b ) { return a.channelID() < b.channelID(); } );
  }
  if ( msgLevel( MSG::DEBUG ) ) {
    for ( auto& cl : pool ) {
      info() << "----" << endmsg;
      info() << cl << endmsg;
      info() << " [fx,fy] " << cl.xfraction() << "," << cl.yfraction() << endmsg;
      info() << " [x,y,z] " << cl.x() << "," << cl.y() << "," << cl.z() << endmsg;
      info() << "pixels" << endmsg;
      for ( auto& pixel : cl.pixels() ) { info() << "\t" << pixel << endmsg; }
    }
  }
  if ( msgLevel( MSG::DEBUG ) ) { info() << "N VPFullCluster produced :  " << pool.size() << endmsg; }

  return result;
}

//=============================================================================
// make RetinaClusters from bank
//=============================================================================
std::vector<LHCb::VPRetinaFullCluster>
VPRetinaFullClustering::makeRetinaFullClusters( LHCb::span<const uint32_t> bank, const unsigned int sensor ) const {
  assert( bank.size() == 1 + bank[0] );

  std::vector<VPRetinaMatrix> RetinaMatrixVector;
  RetinaMatrixVector.reserve( 20 );
  std::vector<LHCb::VPRetinaFullCluster> RetinaCluster;

  // Read super pixel
  for ( const uint32_t sp_word : bank.subspan( 1 ) ) {

    uint8_t sp = sp_word & 0xFFU;

    if ( 0 == sp ) continue; // protect against zero super pixels.

    const uint32_t sp_addr          = ( sp_word & 0x007FFF00U ) >> 8;
    const uint32_t sp_row           = sp_addr & 0x3FU;
    const uint32_t sp_col           = ( sp_addr >> 6 );
    const uint32_t no_sp_neighbours = sp_word & 0x80000000U;

    // if a super pixel is not isolated
    // we use Retina Clustering Algorithm
    if ( !no_sp_neighbours ) {
      // we look for already created Retina
      auto iterRetina = std::find_if( RetinaMatrixVector.begin(), RetinaMatrixVector.end(),
                                      [&]( const VPRetinaMatrix& m ) { return m.IsInRetina( sp_row, sp_col ); } );
      if ( iterRetina != RetinaMatrixVector.end() ) {
        iterRetina->AddSP( sp_row, sp_col, sp );
        continue;
      } else {
        // if we have not reached maximum chain length
        // we create a new retina
        if ( RetinaMatrixVector.size() < m_chain_length ) {
          RetinaMatrixVector.emplace_back( sp_row, sp_col, sp, sensor );
          continue;
        } else {
          // continue;
        }
      }
    }

    // if a super pixel is isolated or the RetinaMatrix chain is full
    // the clustering boils down to a simple pattern look up.

    // there is always at least one cluster in the super
    // pixel. look up the pattern and add it.

    // there is always at least one cluster in the super
    // pixel. look up the pattern and add it.

    // remove after caches rewrite
    const auto&    spcache = s_SPCaches[sp];
    const uint32_t idx     = spcache.pattern;

    const uint32_t row = idx & 0x03U;
    const uint32_t col = ( idx >> 2 ) & 1;

    const uint64_t cX = ( ( sp_col * 2 + col ) << 3 ) + ( uint64_t )( spcache.fxy[0] * 8 );
    const uint64_t cY = ( ( sp_row * 4 + row ) << 3 ) + ( uint64_t )( spcache.fxy[1] * 8 );

    std::vector<LHCb::VPChannelID> channelIDs;
    uint32_t                       shift_r = 0;
    while ( ( ( ( sp >> shift_r ) & 0x1 ) == 0 ) && ( ( ( sp >> ( shift_r + 4 ) ) & 0x1 ) == 0 ) ) shift_r++;
    while ( ( ( ( ( sp >> shift_r ) & 0x1 ) == 1 ) || ( ( ( sp >> ( shift_r + 4 ) ) & 0x1 ) == 1 ) ) && shift_r < 4 ) {
      if ( ( ( sp >> shift_r ) & 0x1 ) == 1 ) {
        const uint32_t id_X    = sp_col * 2;
        const uint32_t id_Y    = sp_row * 4 + shift_r;
        const uint32_t id_chip = id_X / Pixel::CHIP_COLUMNS;
        const uint32_t id_ccol = id_X % Pixel::CHIP_COLUMNS;
        channelIDs.push_back( LHCb::VPChannelID( sensor, id_chip, id_ccol, id_Y ) );
      }
      if ( ( ( sp >> ( shift_r + 4 ) ) & 0x1 ) == 1 ) {
        const uint32_t id_X    = sp_col * 2 + 1;
        const uint32_t id_Y    = sp_row * 4 + shift_r;
        const uint32_t id_chip = id_X / Pixel::CHIP_COLUMNS;
        const uint32_t id_ccol = id_X % Pixel::CHIP_COLUMNS;
        channelIDs.push_back( LHCb::VPChannelID( sensor, id_chip, id_ccol, id_Y ) );
      }
      shift_r++;
    }
    RetinaCluster.push_back( LHCb::VPRetinaFullCluster( cX << 11 | cY, channelIDs ) );
    if ( idx & 8 ) {
      const uint32_t row = ( idx >> 4 ) & 3;
      const uint32_t col = ( idx >> 6 ) & 1;

      const uint64_t cX = ( ( sp_col * 2 + col ) << 3 ) + ( uint64_t )( spcache.fxy[2] * 8 );
      const uint64_t cY = ( ( sp_row * 4 + row ) << 3 ) + ( uint64_t )( spcache.fxy[3] * 8 );

      std::vector<LHCb::VPChannelID> channelIDs;
      while ( ( ( ( sp >> shift_r ) & 0x1 ) == 0 ) && ( ( ( sp >> ( shift_r + 4 ) ) & 0x1 ) == 0 ) ) shift_r++;
      while ( ( ( ( ( sp >> shift_r ) & 0x1 ) == 1 ) || ( ( ( sp >> ( shift_r + 4 ) ) & 0x1 ) == 1 ) ) &&
              shift_r < 4 ) {
        if ( ( ( sp >> shift_r ) & 0x1 ) == 1 ) {
          const uint32_t id_X    = sp_col * 2;
          const uint32_t id_Y    = sp_row * 4 + shift_r;
          const uint32_t id_chip = id_X / Pixel::CHIP_COLUMNS;
          const uint32_t id_ccol = id_X % Pixel::CHIP_COLUMNS;
          channelIDs.push_back( LHCb::VPChannelID( sensor, id_chip, id_ccol, id_Y ) );
        }
        if ( ( ( sp >> ( shift_r + 4 ) ) & 0x1 ) == 1 ) {
          const uint32_t id_X    = sp_col * 2 + 1;
          const uint32_t id_Y    = sp_row * 4 + shift_r;
          const uint32_t id_chip = id_X / Pixel::CHIP_COLUMNS;
          const uint32_t id_ccol = id_X % Pixel::CHIP_COLUMNS;
          channelIDs.push_back( LHCb::VPChannelID( sensor, id_chip, id_ccol, id_Y ) );
        }
        shift_r++;
      }
      RetinaCluster.push_back( LHCb::VPRetinaFullCluster( cX << 11 | cY, channelIDs ) );
    }
  }

  // searchRetinaCluster
  for ( auto& m : RetinaMatrixVector ) {
    const auto& clusters = m.SearchFullCluster();
    RetinaCluster.insert( end( RetinaCluster ), begin( clusters ), end( clusters ) );
  };

  return RetinaCluster;
}
