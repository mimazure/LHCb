###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# - Locate GRAPHVIZ library
# Defines:
#
#  GRAPHVIZ_FOUND
#  GRAPHVIZ_INCLUDE_DIR
#  GRAPHVIZ_INCLUDE_DIRS (not cached)
#  GRAPHVIZ_LIBRARIES (not cached)
#  GRAPHVIZ_LIBRARY_DIRS (not cached)
#  GRAPHVIZ_BINARY_PATH (not cached)
#
# Imports:
#
#  Graphviz::graphviz
#  Graphviz::cdt
#  Graphviz::dot (executable)
#
# Usage of the target instead of the variables is advised

# Enforce a minimal list if none is explicitly requested
if(NOT Graphviz_FIND_COMPONENTS)
  set(Graphviz_FIND_COMPONENTS cdt)
endif()

# Find quietly if already found before
if(DEFINED CACHE{CPPUNIT_INCLUDE_DIR})
  set(${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY YES)
endif()

find_path(GRAPHVIZ_INCLUDE_DIR NAMES graphviz/graphviz_version.h)
set(GRAPHVIZ_INCLUDE_DIRS ${GRAPHVIZ_INCLUDE_DIR})

set(GRAPHVIZ_LIBRARY_DIRS)

foreach(component ${Graphviz_FIND_COMPONENTS})
  string(TOUPPER ${component} COMPONENT)
  find_library(GRAPHVIZ_${COMPONENT}_LIBRARY NAMES ${component})
  if(GRAPHVIZ_${COMPONENT}_LIBRARY)
    set(GRAPHVIZ_${COMPONENT}_FOUND 1)
    list(APPEND GRAPHVIZ_LIBRARIES ${GRAPHVIZ_${COMPONENT}_LIBRARY})

    get_filename_component(libdir ${GRAPHVIZ_${COMPONENT}_LIBRARY} PATH)
    list(APPEND GRAPHVIZ_LIBRARY_DIRS ${libdir})
  else()
    set(GRAPHVIZ_${COMPONENT}_FOUND 0)
  endif()
  mark_as_advanced(GRAPHVIZ_${COMPONENT}_LIBRARY)
endforeach()

if(GRAPHVIZ_LIBRARY_DIRS)
  list(REMOVE_DUPLICATES GRAPHVIZ_LIBRARY_DIRS)
endif()

find_program(GRAPHVIZ_DOT_EXECUTABLE dot)

# handle the QUIETLY and REQUIRED arguments and set GRAPHVIZ_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Graphviz DEFAULT_MSG GRAPHVIZ_INCLUDE_DIR GRAPHVIZ_LIBRARIES GRAPHVIZ_DOT_EXECUTABLE)

mark_as_advanced(GRAPHVIZ_FOUND GRAPHVIZ_INCLUDE_DIR GRAPHVIZ_LIBRARIES GRAPHVIZ_DOT_EXECUTABLE)

# Modernisation: create an interface target to link against
if(TARGET Graphviz::graphviz)
    return()
endif()
if(GRAPHVIZ_FOUND)
  add_library(Graphviz::graphviz IMPORTED INTERFACE)
  target_include_directories(Graphviz::graphviz SYSTEM INTERFACE "${GRAPHVIZ_INCLUDE_DIRS}")
  target_link_libraries(Graphviz::graphviz INTERFACE "${GRAPHVIZ_LIBRARIES}")
  # Display the imported target for the user to know
  if(NOT ${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
    message(STATUS "  Import target: Graphviz::graphviz")
  endif()

  foreach(component ${Graphviz_FIND_COMPONENTS})
    string(TOUPPER ${component} COMPONENT)
    add_library(Graphviz::${component} IMPORTED INTERFACE)
    target_include_directories(Graphviz::${component} SYSTEM INTERFACE "${GRAPHVIZ_INCLUDE_DIR}")
    target_link_libraries(Graphviz::${component} INTERFACE "${GRAPHVIZ_${COMPONENT}_LIBRARY}")
    if(NOT ${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
    endif()
  endforeach()

  add_executable(Graphviz::dot IMPORTED)
  set_target_properties(Graphviz::dot PROPERTIES IMPORTED_LOCATION ${GRAPHVIZ_DOT_EXECUTABLE})
  if(NOT ${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
    message(STATUS "  Import target: Graphviz::dot")
  endif()
endif()

if(COMMAND __deprecate_var_for_target)
  foreach(v IN ITEMS GRAPHVIZ_INCLUDE_DIR GRAPHVIZ_INCLUDE_DIRS GRAPHVIZ_LIBRARY GRAPHVIZ_LIBRARIES)
    variable_watch(${v} __deprecate_var_for_target)
  endforeach()
endif()

# Setting variables for the environment.
if(GRAPHVIZ_FOUND)
  get_filename_component(GRAPHVIZ_BINARY_PATH ${GRAPHVIZ_DOT_EXECUTABLE} DIRECTORY)
endif()
